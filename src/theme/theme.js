import { createMuiTheme } from "@material-ui/core";

const primary = "#12203E";
const secondary = "#3677F6";

export default createMuiTheme({
  palette: {
    primary: {
      main: primary,
    },
    secondary: {
      main: secondary,
    },
    background: {
      default: "#EDF2F7",
    },
    text: {
      primary: "#414863",
      secondary: "#4A4A4A",
    },
  },
  typography: {
    fontFamily: "Ubuntu, sans-serif",
    h4: {
      fontFamily: "Poppins",
      fontWeight: 700,
    },
    h5: {
      fontWeight: 600,
    },
    h6: {
      fontWeight: 600,
    },
    body1: {
      fontSize: "14px",
    },
    body2: {
      fontSize: "15px",
      fontWeight: 600,
    },
  },
  overrides: {
    MuiButton: {
      contained: {
        height: "45px",
        padding: "5px 30px",
        textTransform: "capitalize",
        fontWeight: 600,
        borderRadius: "25px",
      },
      label: {
        textTransform: "capitalize",
      },
    },
    MuiFilledInput: {
      root: {
        backgroundColor: "#fff",
        height: "45px",
        borderTopLeftRadius: "30px",
        borderTopRightRadius: "30px",
        borderRadius: "30px",

        "& > input": {
          "&::placeholder": {
            color: primary,
            fontWeight: 400,
          },
        },
        "&:hover": {
          backgroundColor: "#fff",
        },
        "&:focus": {
          backgroundColor: "#fff",
        },
      },
      input: {
        padding: "15px 15px 13px",
      },
    },
    MuiSvgIcon: {
      root: {
        fontSize: "1.25rem",
      },
    },
    MuiListItem: {
      gutters: {
        paddingLeft: "24px",
        paddingRight: "24px",
      },
      button: {
        "&:hover": {
          // backgroundColor: "#283654",
        },
      },
    },
    MuiListItemText: {
      root: {
        marginTop: "0px",
        marginBottom: "0px",
      },
    },
    MuiListItemIcon: {
      root: {
        color: "#fff",
        minWidth: "36px",
      },
    },
    MuiTableHead: {
      root: {
        backgroundColor: "#F7FAFC",
      },
    },
    MuiTableContainer: {
      root: {
        boxShadow: "0px 5px 25px rgba(0, 0, 0, 0.06)",
      },
    },
  },
  props: {
    MuiButton: {
      disableElevation: true,
    },
  },
});
