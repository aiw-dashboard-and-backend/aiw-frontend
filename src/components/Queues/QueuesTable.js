import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Paper,
  makeStyles,
} from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles({
  badge: {
    padding: "5px",
    width: "70px",
    fontSize: "13.5px",
    borderRadius: "25px",
    color: "#363636",
    backgroundColor: "#eee",
    textAlign: "center",
    fontWeight: "bold",
    textTransform: "capitalize",

    "&-Fulfilled": {
      backgroundColor: "#41CCB2",
      color: "#fff",
    },

    "&-Cancelled": {
      backgroundColor: "#FF3131",
      color: "#fff",
    },
    "&-Paused": {
      color: "#fff",
      backgroundColor: "#F9A90B",
    },
  },
});

export default (props) => {
  const classes = useStyles();
  return (
    <TableContainer elevation={0} component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Machine</TableCell>
            <TableCell>Provision</TableCell>
            <TableCell>Type</TableCell>
            <TableCell>Status</TableCell>
            <TableCell>Active</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.queues.map((queue) => (
            <TableRow key={queue.name}>
              <TableCell>{queue.name}</TableCell>
              <TableCell>{queue.machine}</TableCell>
              <TableCell>{queue.provision}</TableCell>
              <TableCell>{queue.type}</TableCell>
              <TableCell>
                <Typography
                  className={clsx(
                    classes.badge,
                    `${classes.badge}-${queue.status}`
                  )}
                >
                  {queue.status}
                </Typography>
              </TableCell>
              <TableCell>{queue.active}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
