import React, { useState } from "react";
import { Box, Button, FilledInput } from "@material-ui/core";
import PageHeader from "../common/PageHeader";

import MenuList from "../common/MenuList";
import QueuesTable from "./QueuesTable";
import { Search as SearchIcon } from "@material-ui/icons";

const queues = [
  {
    name: "User",
    machine: "SRV & TPA Bot 1",
    provision: "Lead Generation",
    type: "Finances",
    status: "Fulfilled",
    active: "May 10, 2020",
  },
  {
    name: "User 2",
    machine: "NDA",
    provision: "Lead Generation",
    type: "Finances",
    status: "Fulfilled",
    active: "May 10, 2020",
  },
  {
    name: "User 3",
    machine: "GDP",
    provision: "Testing Generation",
    type: "Business",
    status: "Paused",
    active: "May 10, 2020",
  },
  {
    name: "User",
    machine: "SRV & TPA Bot 1",
    provision: "Lead Generation",
    type: "Finances",
    status: "Cancelled",
    active: "May 10, 2020",
  },
  {
    name: "User 4",
    machine: "SRV 2",
    provision: "Lead Generation",
    type: "Technical",
    status: "Fulfilled",
    active: "May 10, 2020",
  },
];

export default (props) => {
  const [jobPriorAnchor, setJobPriorAnchor] = useState(null);
  const [jobTypeAnchor, setJobTypeAnchor] = useState(null);
  const openMenu = (e, menu) => {
    console.log(menu);
    switch (menu) {
      case "jobPriority":
        return setJobPriorAnchor(e.currentTarget);
      case "jobType":
        return setJobTypeAnchor(e.currentTarget);
      default:
        return;
    }
  };

  return (
    <>
      <PageHeader title="Queues" />
      <Box display="flex">
        <FilledInput
          endAdornment={<SearchIcon />}
          disableUnderline
          placeholder="Search"
        />
        <Box ml={2}>
          <Button color="secondary" variant="contained">
            Add
          </Button>
        </Box>
        <Box ml="auto">
          <MenuList
            anchor={jobPriorAnchor}
            openAnchor={(e) => openMenu(e, "jobPriority")}
            closeAnchor={() => setJobPriorAnchor(null)}
            options={["All", "Important", "Urgent"]}
            label="Job Priority"
          />
          <MenuList
            anchor={jobTypeAnchor}
            openAnchor={(e) => openMenu(e, "jobType")}
            closeAnchor={() => setJobTypeAnchor(null)}
            options={["All", "Amazing"]}
            label="Job Type"
          />
        </Box>
      </Box>
      <Box mt={3}>
        <QueuesTable queues={queues} />
      </Box>
    </>
  );
};
