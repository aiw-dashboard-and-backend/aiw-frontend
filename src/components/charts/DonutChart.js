import React, { Component } from 'react'

import {
    PieChart, Pie, Sector, Cell, ResponsiveContainer
} from 'recharts';

const renderActiveShape = (props) => {

    // console.log(props);

    const {
        cx, cy, innerRadius, outerRadius, startAngle, endAngle,
        payload, percent, value, name
    } = props;

    return (
        <g>
            <text x={cx} y={cy - 20} dy={8} textAnchor="middle" fill={'white'}>{payload.botName}</text>
            <text x={cx} y={cy + 20} dy={8} textAnchor="middle" fill={'white'}>{value} Tasks Processed ({(percent * 100).toFixed(0)}%)</text>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill={COLORS[name % COLORS.length]}
                stroke={COLORS[name % COLORS.length]}
            />
            <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={COLORS[name % COLORS.length]}
                stroke={COLORS[name % COLORS.length]}
            />
        </g>
    );
};


const COLORS = ['#fcbe2d', '#30419b', '#02c58d','#30419b', '#00C49F'];

export default class DonutChart extends Component {

    getPie = () => {
        let pies = [];

        this.props.pieData.forEach((entry, index) => {

            pies.push(<Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]}  />)
        })

        return pies;
    }

    state = {
        activeIndex: 0,
    };

    onPieEnter = (data, index) => {
        this.setState({
            activeIndex: index,
        });
    };

    componentDidUpdate = () => {
        this.getPie();
    }

    render() {
        return (
            <div className="card m-b-30">
                <div className="card-body">
                    <h4 className="mt-0 header-title mb-4">Task Distribution</h4>
                    <div style={{ width: '100%', height: 350 }}>
                        <ResponsiveContainer>
                            <PieChart>
                                <Pie
                                    activeIndex={this.state.activeIndex}
                                    activeShape={renderActiveShape}
                                    data={this.props.pieData}
                                    cx={175}
                                    cy={175}
                                    innerRadius={115}
                                    outerRadius={150}
                                    dataKey="value"
                                    onMouseEnter={this.onPieEnter}
                                >
                                {this.getPie()}
                                </Pie>
                            </PieChart>
                        </ResponsiveContainer>
                    </div>
                </div>
            </div>
        )
    }
}
