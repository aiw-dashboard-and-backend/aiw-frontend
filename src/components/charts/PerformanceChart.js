import React, { Component } from 'react'
import {
  AreaChart, Area, XAxis, YAxis, Tooltip, ResponsiveContainer, Legend
} from 'recharts';

export default class PerformanceChart extends Component {

    getAreas = () => {

      let areas = [];

      this.props.botNames.forEach((name, i) => {
        if(name !== 'noBot'){
          areas.push(<Area key={name} type="monotone" dataKey={name} stackId="1" stroke={this.props.lineColors[i]} fill={this.props.lineColors[i]} fillOpacity={0.75} />)
        }
      })

      return areas;
    }

    componentDidUpdate = () => {
      this.getAreas();
    }

    render() {
        return (
            <div className="card m-b-30">
                <div className="card-body">
                    <h4 className="mt-0 header-title mb-4">Individual Bot Performance</h4>
                    <div style={{width: '100%', height: 350 }} >
                      <ResponsiveContainer>
                        <AreaChart
                            data={this.props.chartData}
                            margin={{
                              top: 10, right: 30, left: 0, bottom: 0,
                            }}
                        >
                            {/* <XAxis label="Time of Day" dataKey="hour" /> */}
                            <XAxis dataKey="hour" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            {this.getAreas()}
                        </AreaChart>
                      </ResponsiveContainer>
                    </div>
                </div>
            </div>
        )
    }
}
