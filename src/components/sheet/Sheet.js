import React, { Component } from "react";
import WebDataRocksReact from "webdatarocks";
// import WebDataRocksReact from "webdatarocks";

export default class Sheet extends Component {
  testJSON = [
    {
      start_time: 234,
      end_time: 123,
      "claim id": 54,
      "ip number": 67,
      date: "04/30/2020",
      status: "successful",
    },
  ];

  state = {
    wdr: null,
  };

  componentDidMount = () => {
    const resp = new WebDataRocksReact({
      container: "#wdr-component",
      beforetoolbarcreated: this.customizeToolbar,
      toolbar: true,
      height: 600,
      width: 1200,
      report: {
        dataSource: {
          filename: "http://68.183.231.207:8001/api/" + this.props.endpoint,
          fieldSeparator: ",",
          //   data: this.testJSON,
        },
        options: {
          grid: {
            type: "flat",
            showGrandTotals: "off",
          },
        },
      },
    });
    this.setState({ wdr: resp });
  };
  customizeToolbar(toolbar) {
    var tabs = toolbar.getTabs();
    toolbar.getTabs = function () {
      // var menu = tabs[0].menu;
      // delete menu[1];
      // delete menu[2];
      delete tabs[0];
      delete tabs[1];
      delete tabs[2];
      delete tabs[7];
      return tabs;
    };
  }
  render() {
    return (
      <React.Fragment>
        <div
          className="container center"
          id="wdr-component"
          ref={this.div}
        ></div>
      </React.Fragment>
    );
  }
}
