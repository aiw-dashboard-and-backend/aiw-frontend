import React, { Component } from "react";

import { connect } from "react-redux";

import Login from "../../pages/auth/Login";

export class Startup extends Component {
  isAuthenticated = () => {
    if (this.props.auth) {
      return !(
        Object.entries(this.props.auth).length === 0 &&
        this.props.auth.constructor === Object
      );
    } else {
      return false;
    }
  };

  render() {
    return this.isAuthenticated() ? this.props.children : <Login />;
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth.Auth,
  };
}

export default connect(mapStateToProps, {})(Startup);
