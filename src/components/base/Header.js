import React, { Component } from "react";

import { Link, NavLink, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import { logoutUser } from "../../redux/actions/authActions";

import UserPNG from "../assets/images/users/user-4.jpg";

class Header extends Component {
  state = {
    location: this.props.location.pathname,
  };

  callLogout = (e) => {
    this.props.logoutUser();
  };

  render() {
    return (
      <React.Fragment>
        <div className="header-bg">
          {/* Navigation Bar*/}
          <header id="topnav">
            <div className="topbar-main">
              <div className="container-fluid">
                {/* Logo*/}
                <div>
                  <NavLink to="/" className="logo">
                    <span className="logo-light">
                      <i className="mdi mdi-camera-control" /> AIW
                    </span>
                  </NavLink>
                </div>
                {/* End Logo*/}
                <div className="menu-extras topbar-custom navbar p-0">
                  {/* <ul className="list-inline d-none d-lg-block mb-0">
                                        <li className="hide-phone app-search float-left">
                                            <form role="search" className="app-search">
                                                <div className="form-group mb-0">
                                                    <input type="text" className="form-control" placeholder="Search.." />
                                                    <button type="submit"><i className="fa fa-search" /></button>
                                                </div>
                                            </form>
                                        </li>
                                    </ul> */}
                  <ul className="navbar-right ml-auto list-inline float-right mb-0">
                    {/* full screen */}
                    <li className="dropdown notification-list list-inline-item">
                      <div className="dropdown notification-list nav-pro-img">
                        <Link
                          className="dropdown-toggle nav-link arrow-none nav-user"
                          to="/account"
                        >
                          <img
                            src={UserPNG}
                            alt="user"
                            className="rounded-circle"
                          />
                        </Link>
                        <div className="dropdown-menu dropdown-menu-right profile-dropdown ">
                          {/* item*/}
                          {/* <a className="dropdown-item" href="index.html"><i className="mdi mdi-account-circle" /> Profile</a>
                                                    <a className="dropdown-item d-block" href="index.html"><i className="mdi mdi-settings" /> Settings</a> */}
                          {/* <div className="dropdown-divider" /> */}
                          <button
                            className="dropdown-item text-danger"
                            onClick={(e) => this.callLogout(e)}
                          >
                            <i className="mdi mdi-power text-danger" /> Logout
                          </button>
                        </div>
                      </div>
                    </li>
                    <li className="menu-item dropdown notification-list list-inline-item">
                      {/* Mobile menu toggle*/}
                      <a className="navbar-toggle nav-link" href="index.html">
                        <div className="lines">
                          <span />
                          <span />
                          <span />
                        </div>
                      </a>
                      {/* End mobile menu toggle*/}
                    </li>
                  </ul>
                </div>
                {/* end menu-extras */}
                <div className="clearfix" />
              </div>
              {/* end container */}
            </div>
            {/* end topbar-main */}
            {/* MENU Start */}
            <div className="navbar-custom">
              <div className="container-fluid">
                <div id="navigation">
                  {/* Navigation Menu*/}
                  <ul className="navigation-menu">
                    <li
                      className={
                        this.state.location === "/" ? "has-submenu active" : ""
                      }
                    >
                      <Link to="/">
                        <i className="fas fa-tachometer-alt" /> Dashboard
                      </Link>
                    </li>
                    <li
                      className={
                        this.state.location === "/vault"
                          ? "has-submenu active"
                          : ""
                      }
                    >
                      <Link to="/vault">
                        <i className="fas fa-key" /> Vault
                      </Link>
                    </li>
                    <li
                      className={
                        this.state.location === "/bots" ||
                        this.state.location === "/bots/1"
                          ? "has-submenu active"
                          : ""
                      }
                    >
                      <Link to="/bots">
                        <i className="fas fa-robot" /> Bots
                      </Link>
                    </li>
                    <li className="has-submenu">
                      <a href="#">
                        <i className="fas fa-chart-area" /> Reports{" "}
                        <i className="mdi mdi-chevron-down mdi-drop"></i>
                      </a>
                      <ul className="submenu">
                        <li>
                          <ul>
                            <li>
                              <Link to="/reports/intimation">
                                {" "}
                                Bill Intimation
                              </Link>
                            </li>
                            <li>
                              <Link to="/reports/submission">
                                {" "}
                                Bill Submission
                              </Link>{" "}
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </li>
                  </ul>
                  {/* End navigation menu */}
                </div>
                {/* end #navigation */}
              </div>
              {/* end container */}
            </div>
            {/* end navbar-custom */}
          </header>
          {/* End Navigation Bar*/}
        </div>
        {/* header-bg */}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({});

export default withRouter(connect(mapStateToProps, { logoutUser })(Header));
