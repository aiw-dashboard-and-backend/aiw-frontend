import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <React.Fragment>
                {/* Footer */}
                <footer className="footer">
                © {(new Date().getFullYear())} AIW Control Panel  <span className="d-none d-sm-inline-block"> - Developed with <i className="mdi mdi-heart text-danger" /> by FernTech Solutions Ltd.</span>.
                </footer>
                {/* End Footer */}
            </React.Fragment>
        )
    }
}
