import React, { Component } from 'react'

import { fetchDashboardActivity, decryptLogsWebSocket } from '../../redux/actions/initActions'
import { connect } from 'react-redux'

import ActivityItem from './ActivityItem'

class ActivityList extends Component {

    render() {
        
        if(this.props.encrypted.length !== 0) {
            const enc = this.props.encrypted[0].encrypted
            this.props.decryptLogsWebSocket(enc)
        }

        const activityItem = this.props.activity.map((activity, i) => {
            return(
                <ActivityItem 
                    key = {i}
                    botName = {activity.botName}
                    timestamp = {activity.timestamp}
                    message = {activity.message}
                    >
                </ActivityItem>
            );
        });

        return (
            <div className="card m-b-30" id = "activityList" >
                <div className="card-body">
                    <h4 className="mt-0 header-title mb-4">Recent Activity</h4>
                    <ol className="activity-feed mb-0">
                        {activityItem}
                    </ol>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        encrypted: state.init.dashboardActivityEncrypted,
        activity: state.init.dashboardActivity,
        auth: state.auth.Auth
    }
    
}

export default connect(mapStateToProps, { fetchDashboardActivity, decryptLogsWebSocket })(ActivityList)