import React, { Component } from 'react'

export default class ActivityItem extends Component {
    render() {
        return (
            <li className="feed-item">
                <p className="text-muted mb-1">{this.props.timestamp}</p>
                <p className="font-15 mt-0 mb-0">{this.props.botName}: <b>{this.props.message}</b></p>
            </li>
        )
    }
}
