import React, { useState } from "react";
import {
  Drawer,
  Box,
  makeStyles,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Collapse,
} from "@material-ui/core";
import { ExpandMore, ExpandLess } from "@material-ui/icons";
import { useHistory, NavLink } from "react-router-dom";

import links from "./links";

const useStyles = makeStyles((theme) => ({
  drawer: {
    backgroundColor: theme.palette.primary.main,
    color: "#fff",
    border: "none",
    overflowY: "scroll",

    "&::-webkit-scrollbar": {
      width: "0px",
      background: "transparent",
    },
  },
  nested: {
    paddingLeft: theme.spacing(7),
    paddingRight: 0,
  },
  opened: {
    backgroundColor: "#283654",
    borderRight: "5px solid #92E6FC",
    marginBottom: theme.spacing(1),
  },
  notOpened: {
    borderRight: "5px solid rgba(0,0,0,0)",
    marginBottom: theme.spacing(1),
  },
  subheadText: {
    marginRight: theme.spacing(1.5),
  },
}));

const SideNav = (props) => {
  const classes = useStyles();
  const [opened, setOpened] = useState([]);
  const history = useHistory();

  const toggleNav = (sub) => {
    opened.includes(sub)
      ? setOpened((o) => o.filter((l) => l !== sub))
      : setOpened((o) => o.concat(sub));
  };

  return (
    <Drawer
      classes={{
        paper: classes.drawer,
      }}
      variant="permanent"
    >
      <Box textAlign="center" pt={5} pb={2.5}>
        <img src="/assets/images/aiw_logo.png" alt="AIW" />
      </Box>
      <Box>
        <List>
          {links.map((link) => (
            <Box key={Math.random()}>
              <ListItem
                className={
                  opened.includes(link.title)
                    ? classes.opened
                    : classes.notOpened
                }
                button
                onClick={() =>
                  link.nested ? toggleNav(link.title) : history.push(link.link)
                }
              >
                <ListItemIcon>
                  <link.Icon />
                </ListItemIcon>
                <ListItemText
                  className={classes.subheadText}
                  primary={link.title}
                />
                {!!link.nested &&
                  (opened.includes(link.title) ? (
                    <ExpandLess />
                  ) : (
                    <ExpandMore />
                  ))}
              </ListItem>
              {link.nested && (
                <Collapse in={opened.includes(link.title)} unmountOnExit>
                  <List component="div" disablePadding>
                    {link.nested.map((nested) => (
                      <ListItem
                        key={Math.random()}
                        button
                        component={NavLink}
                        to={nested.link}
                        className={classes.nested}
                      >
                        <ListItemText primary={nested.title} />
                      </ListItem>
                    ))}
                  </List>
                </Collapse>
              )}
            </Box>
          ))}
        </List>
      </Box>
    </Drawer>
  );
};

export default SideNav;
