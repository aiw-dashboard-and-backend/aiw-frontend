import {
  Pageview,
  Autorenew,
  SettingsInputComponent,
  Ballot,
  Dashboard,
  Settings,
} from "@material-ui/icons";
export default [
  {
    title: "Dashboard",
    Icon: Dashboard,
    link: "/",
  },
  {
    title: "Monitoring",
    Icon: Pageview,
    nested: [
      {
        title: "Robot",
        link: "/",
      },
      {
        title: "Jobs",
        link: "/",
      },
      {
        title: "Queues",
        link: "/",
      },
      {
        title: "Logs",
        link: "/",
      },
    ],
  },
  {
    title: "Automation",
    Icon: Autorenew,
    nested: [
      {
        title: "Processes",
        link: "/processes",
      },
      {
        title: "Triggers",
        link: "/triggers",
      },
      {
        title: "Queues",
        link: "/queues",
      },
      {
        title: "Storage Bucket",
        link: "/",
      },
    ],
  },
  {
    title: "Testing",
    Icon: SettingsInputComponent,
    link: "/testing",
  },
  {
    title: "Management",
    Icon: Ballot,
    nested: [
      {
        title: "Users",
        link: "/",
      },
      {
        title: "Queues",
        link: "/",
      },
      {
        title: "Robots",
        link: "/",
      },
      {
        title: "Package",
        link: "/",
      },
      {
        title: "Machines",
        link: "/",
      },
    ],
  },
  {
    title: "Settings",
    Icon: Settings,
    nested: [
      {
        title: "User",
        link: "/",
      },
      {
        title: "Account",
        link: "/",
      },
    ],
  },
];
