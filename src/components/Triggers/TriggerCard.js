import React from "react";
import { Box, IconButton, Typography, makeStyles } from "@material-ui/core";
import {
  MoreHoriz as MoreHorizIcon,
  Timer as TimerIcon,
} from "@material-ui/icons";
import clsx from "clsx";

const useStyles = makeStyles((theme) => ({
  badge: {
    backgroundColor: theme.palette.grey[200],
    color: theme.palette.grey[700],
    fontWeight: 600,
    padding: "5px 15px",
    borderRadius: "25px",
  },
  wrapper: {
    backgroundImage: (props) => `url(/assets/images/trigger-${props.type}.svg)`,
    backgroundRepeat: "no-repeat",
    backgroundPosition: "right bottom",
    cursor: "pointer",
    transition: ".3s",
    "&:hover": {
      boxShadow: "0px 5px 25px rgba(0, 0, 0, 0.1)",
    },
    "&-Successful": {
      borderLeft: (props) => "8px solid #41CCB2",
    },
    "&-Pending": {
      borderLeft: (props) => "8px solid #F9A90B",
    },
  },
  state: {
    "&-Successful": {
      fontWeight: 600,
      color: "#41CCB2",
    },
    "&-Pending": {
      fontWeight: 600,
      color: "#F9A90B",
    },
  },
}));

export default (props) => {
  const classes = useStyles({ type: props.type });
  return (
    <Box
      className={clsx(classes.wrapper, `${classes.wrapper}-${props.state}`)}
      bgcolor="white"
      p={2}
      borderRadius="5px"
    >
      <Box display="flex" justifyContent="space-between">
        <Typography className={classes.badge}>Type: {props.type}</Typography>
        <IconButton size="small">
          <MoreHorizIcon />
        </IconButton>
      </Box>
      <Box my={1}>
        <Typography variant="h6">{props.name}</Typography>
      </Box>
      <Box mb={1}>
        <Typography variant="body1">Env: {props.env}</Typography>
      </Box>
      <Box display="flex" alignItems="center">
        <Box mr={1}>
          <img src="/assets/images/icons/bot.svg" alt="Bot" />
        </Box>
        <Typography>{props.bot}</Typography>
      </Box>
      <Box my={1} display="flex" alignItems="center">
        <Box mr={1} display="flex" alignItems="center">
          <TimerIcon style={{ fontSize: "15px" }} />
        </Box>
        <Typography>{props.triggerAt}</Typography>
      </Box>
      <Typography variant="body2">
        State:{" "}
        <Typography
          component="span"
          className={`${classes.state}-${props.state}`}
        >
          {props.state}
        </Typography>
      </Typography>
    </Box>
  );
};
