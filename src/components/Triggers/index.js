import React, { useState } from "react";
import { Box, Button, FilledInput, Grid } from "@material-ui/core";
import PageHeader from "../common/PageHeader";

import MenuList from "../common/MenuList";
import { Search as SearchIcon } from "@material-ui/icons";
import TriggerCard from "./TriggerCard";

const triggers = [
  {
    id: 1,
    type: "Time",
    name: "WriteToNotepad",
    env: "Development",
    bot: "NID Verification",
    triggerAt: "Every min",
    state: "Successful",
  },
  {
    id: 2,
    type: "Queue",
    name: "AddedToQueue",
    env: "Production",
    bot: "NID Verification",
    triggerAt: "Every day",
    state: "Pending",
  },
  {
    id: 3,
    type: "Mail",
    name: "SendEmail",
    env: "Development",
    bot: "SRC Verification",
    triggerAt: "Every Tuesday",
    state: "Successful",
  },
  {
    id: 4,
    type: "Time",
    name: "WriteToNotepad",
    env: "Production",
    bot: "GRD",
    triggerAt: "Every min",
    state: "Pending",
  },
];

export default (props) => {
  const [jobPriorAnchor, setJobPriorAnchor] = useState(null);
  const [jobTypeAnchor, setJobTypeAnchor] = useState(null);
  const openMenu = (e, menu) => {
    console.log(menu);
    switch (menu) {
      case "jobPriority":
        return setJobPriorAnchor(e.currentTarget);
      case "jobType":
        return setJobTypeAnchor(e.currentTarget);
      default:
        return;
    }
  };

  return (
    <>
      <PageHeader title="Triggers" />
      <Box display="flex">
        <FilledInput
          endAdornment={<SearchIcon />}
          disableUnderline
          placeholder="Search"
        />
        <Box ml={2}>
          <Button color="secondary" variant="contained">
            Add
          </Button>
        </Box>
        <Box ml="auto">
          <MenuList
            anchor={jobPriorAnchor}
            openAnchor={(e) => openMenu(e, "jobPriority")}
            closeAnchor={() => setJobPriorAnchor(null)}
            options={["All", "Important", "Urgent"]}
            label="State"
          />
          <MenuList
            anchor={jobTypeAnchor}
            openAnchor={(e) => openMenu(e, "jobType")}
            closeAnchor={() => setJobTypeAnchor(null)}
            options={["All", "Amazing"]}
            label="Source"
          />
          <MenuList
            anchor={jobTypeAnchor}
            openAnchor={(e) => openMenu(e, "jobType")}
            closeAnchor={() => setJobTypeAnchor(null)}
            options={["All", "Amazing"]}
            label="Interval"
          />
        </Box>
      </Box>
      <Box mt={3}>
        <Grid container spacing={3}>
          {triggers.map((trigger) => (
            <Grid item xs={4} key={trigger.id}>
              <TriggerCard {...trigger} />
            </Grid>
          ))}
        </Grid>
      </Box>
    </>
  );
};
