import React, { Component } from 'react'

import ActionButton from './ActionButton'

export default class BotActionCard extends Component {
    render() {
        return (
        <div className="card m-b-30">
            <div className="card-body">
                <h4 className="mt-0 header-title mb-4">Actions</h4>
                <div className="row mb-3">
                    <div className="col-md-12">
                        <ActionButton></ActionButton>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className="col-md-12">
                        <ActionButton></ActionButton>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className="col-md-12">
                        <ActionButton></ActionButton>
                    </div>
                </div>
                <div className="row mb-3">
                    <div className="col-md-12">
                        <ActionButton></ActionButton>
                    </div>
                </div>
            </div>
        </div>

        )
    }
}
