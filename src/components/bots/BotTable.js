import React, { Component } from 'react'
import { connect } from 'react-redux'
import Cookies from 'js-cookie'
import {
    fetchBotList
} from '../../redux/actions/botActions'

import {
    sendAction
} from '../../redux/actions/botActions'

import Datatable from 'react-bs-datatable'

import ActionButton from './ActionButton'

// DataTables CSS
import '../assets/plugins/datatables/dataTables.bootstrap4.min.css'
import '../assets/plugins/datatables/responsive.bootstrap4.min.css'

class BotTable extends Component {

    actions = [
        <ActionButton key="1" action="Play" fullClass="mdi mdi-play" colorway="btn-success" />,
        <ActionButton key="2" action="Stop" fullClass="mdi mdi-stop" colorway="btn-danger"/>,
        <ActionButton key="3" action="View Live" fullClass="mdi mdi-video" colorway="btn-outline-info"/>,
        <ActionButton key="4" action="View Logs" fullClass="mdi mdi-file-document" colorway="btn-outline-warning"/>,
    ]

    tableHeaders = [
        {
            title: 'Unit ID',
            prop: 'unitID',
            filterable: true,
            sortable: true
        },
        {
            title: 'Name',
            prop: 'botName',
            filterable: true,
            sortable: true
        },
        {
            title: 'State',
            prop: 'botState',
            filterable: true,
            sortable: true
        },
        {
            title: 'Tasks Completed',
            prop: 'tasksComplete',
            filterable: true,
            sortable: true
        },
        {
            title: 'Runtime',
            prop: 'runtime',
            filterable: true,
            sortable: true
        },
        {
            title: 'Actions',
            prop: 'actions',
        }                            
    ];

    componentDidMount = () => {
        this.props.fetchBotList()
    }

    handleActionClick = (actionType, e) => {

        let tokenData = Cookies.get('aiw_token')
        tokenData = JSON.parse(tokenData)

        const data = {
            'action': actionType,
            'auth_token': tokenData.access_token
        }
        this.props.sendAction(data)
    }

    handleViewLive = (e, i) => {
        window.open("http://103.93.177.178:608"+i+"/vnc_lite.html?password=secret")
    }

    render() {

        const tableBody = this.props.botList.map((data, i) => {
            return({
                unitID: data.unitID,
                botName: data.botName,
                botState: <span className={data.runBadge}>{data.runText}</span>,
                tasksComplete: data.tasksCompleted,
                runtime: data.totalRuntime,
                actions: [
                    <ActionButton 
                        key={data.unitID+"_play"} 
                        action="Play" 
                        fullClass="mdi mdi-play" 
                        colorway="btn-success" 
                        disabled={(data.runText === 'Running') ? true : false }
                        onClick={(e) => this.handleActionClick(data.unitID+"_play", e)}
                    />, 
                    <ActionButton 
                        key={data.unitID+"_stop"} 
                        action="Stop" 
                        fullClass="mdi mdi-stop" 
                        colorway="btn-danger" 
                        disabled={(data.runText === 'Running') ? false : true }
                        onClick={(e) => this.handleActionClick(data.unitID+"_stop", e)}
                    />, 
                    <ActionButton 
                        key={data.unitID+"_live"} 
                        action="View Live" 
                        fullClass="mdi mdi-video" 
                        colorway="btn-outline-warning" 
                        disabled={(data.runText === 'Running') ? false : true }
                        onClick={(e) => this.handleViewLive(e, i)}
                    />, 
                    // <ActionButton key={data.unitID+"_play"} action="View Logs" fullClass="mdi mdi-file-document" colorway="btn-outline-warning"/>,
                ]
            });
        });

        return (
            <div className="card m-b-30">
                <div className="card-body">
                    <h4 className="mt-0 header-title">Provisioned AIW Units</h4>
                    <p className="sub-title">
                        The following table consists of all the bots provisioned within your system - along with
        details like <code>state</code>, <code>entries processed</code> and corresponding <code>action buttons</code>
                    </p>
                    <Datatable
                        tableHeaders={this.tableHeaders}
                        tableBody={tableBody}
                        tableClass="table table-bordered dt-responsive nowrap"
                        rowsPerPage={10}
                        keyName="key"
                    />
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => ({
    botList: state.bots.botList
})

export default connect(
                    mapStateToProps, 
                    {   fetchBotList,
                        sendAction
                    })(BotTable)
