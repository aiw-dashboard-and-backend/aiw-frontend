import React, { Component } from 'react'

export default class ActionButton extends Component {
    render() {
        return (
            <button 
                type="button" 
                className={"btn btn-lg waves-effect waves-light " + this.props.colorway } 
                style={{ width: '100%', margin: 5 }} 
                disabled={this.props.disabled} 
                onClick={this.props.onClick}
            >
                {this.props.action} <i className={this.props.fullClass} />
            </button>
        )
    }
}
