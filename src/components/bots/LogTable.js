import React, { Component } from 'react'
import Datatable from 'react-bs-datatable'

// DataTables CSS
import '../assets/plugins/datatables/dataTables.bootstrap4.min.css'
import '../assets/plugins/datatables/responsive.bootstrap4.min.css'

export default class LogTable extends Component {

    tableHeaders = [
        {
            title: 'ID',
            prop: 'id',
            filterable: true,
            sortable: true
        },
        {
            title: 'Timestamp',
            prop: 'timestamp',
            filterable: true,
            sortable: true
        },
        {
            title: 'Process Name',
            prop: 'processName',
            filterable: true,
            sortable: true
        },
        {
            title: 'Message',
            prop: 'message',
            filterable: true,
            sortable: true
        },
        {
            title: 'Success',
            prop: 'success',
            filterable: true,
            sortable: true
        }                            
    ];

    tableBody = [
        {
            id: '10045',
            timestamp: '2019/03/21 05:21 PM',
            processName: 'Get NID Details',
            message: 'NID retrieved successfully',
            success: <span className="badge badge-success">Success</span>
        },
        {
            id: '10046',
            timestamp: '2019/03/21 05:21 PM',
            processName: 'Get NID Details',
            message: 'Going to NID website',
            success: <span className="badge badge-danger">Failed</span>
        }        
    ];    

    render() {
        return (
            <div className="card m-b-30">
                <div className="card-body">
                    <h4 className="mt-0 header-title">Unit logs</h4>
                    <Datatable
                        tableHeaders={this.tableHeaders}
                        tableBody={this.tableBody}
                        tableClass="table table-bordered dt-responsive nowrap"
                        rowsPerPage={10}
                        keyName="key"
                        // rowsPerPageOption={[5, 10, 15, 20]}                        
                    />
                </div>
            </div>

        )
    }
}
