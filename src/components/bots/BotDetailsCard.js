import React, { Component } from 'react'

export default class BotDetailsCard extends Component {
    render() {
        return (
            <div className="card m-b-30" style={{ padding: 25 }}>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item"> <strong>Unit name:</strong> NID bot</li>
                </ul>
                <div className="card-body">
                    <h4 className="card-title font-16 mt-0">Description</h4>
                    <p className="card-text">Some quick example text to build on the card title and make
    up the bulk of the card's content.</p>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">State: <span className="badge badge-success">Running</span></li>
                    <li className="list-group-item">Provision date: 23rd April, 2019</li>
                    <li className="list-group-item">Processes handled: 43,980</li>
                    <li className="list-group-item">Runtime: 400 H, 20 M</li>
                    <li className="list-group-item">Host IP: 192.168.3.103</li>
                </ul>
            </div>

        )
    }
}
