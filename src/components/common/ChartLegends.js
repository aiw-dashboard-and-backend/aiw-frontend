import React from "react";
import { Box, Typography } from "@material-ui/core";

export default (props) => (
  <>
    <Box display="flex" alignItems="center" mb={1.5}>
      <Box
        mr={1.5}
        display="inline-block"
        height="10px"
        width="10px"
        borderRadius="50%"
        bgcolor="#5C87DC"
      ></Box>
      <Typography variant="body2" color="primary">
        Active
      </Typography>
    </Box>
    <Box display="flex" alignItems="center" mb={1.5}>
      <Box
        mr={1.5}
        display="inline-block"
        height="10px"
        width="10px"
        borderRadius="50%"
        bgcolor="#74E3CE"
      ></Box>
      <Typography variant="body2" color="primary">
        Completed
      </Typography>
    </Box>
    <Box display="flex" alignItems="center">
      <Box
        mr={1.5}
        display="inline-block"
        height="10px"
        width="10px"
        borderRadius="50%"
        bgcolor="#F9DB6D"
      ></Box>
      <Typography variant="body2" color="primary">
        Ended
      </Typography>
    </Box>
  </>
);
