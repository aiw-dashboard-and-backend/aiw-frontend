import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

class PageTitle extends Component {
    render() {

        const breadcrumbs = this.props.crumbs.map((crumb, i, arr) => {
            if(arr.length - 1 === i){
                return (<li className="breadcrumb-item active" key = {crumb.id} >{crumb.name}</li>);
            }else{
                return(<li className="breadcrumb-item" key = {crumb.id} ><Link to = {crumb.path} >{crumb.name}</Link></li>);
            }

        });

        return (
            <div className="page-title-box">
                <div className="row align-items-center">
                    <div className="col-sm-6">
                        <h4 className="page-title">{this.props.title}</h4>
                    </div>
                    <div className="col-sm-6">
                        <ol className="breadcrumb float-right">
                            {breadcrumbs}
                        </ol>
                    </div>
                </div>
            </div>
        )
    }
}

PageTitle.propTypes = {
    title: PropTypes.string.isRequired,
    crumbs: PropTypes.array.isRequired
};

export default PageTitle;