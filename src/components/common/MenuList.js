import React from "react";
import { Button, Menu, Fade, MenuItem } from "@material-ui/core";
import { ExpandMore as ExpandMoreIcon } from "@material-ui/icons";

export default (props) => (
  <>
    <Button
      aria-controls="fade-menu"
      aria-haspopup="true"
      onClick={props.openAnchor}
    >
      {props.label}: {props.options[0]} <ExpandMoreIcon />
    </Button>
    <Menu
      id="fade-menu"
      anchorEl={props.anchor}
      keepMounted
      open={Boolean(props.anchor)}
      onClose={props.closeAnchor}
      TransitionComponent={Fade}
    >
      {props.options.map((opt) => (
        <MenuItem key={opt}>{opt}</MenuItem>
      ))}
    </Menu>
  </>
);
