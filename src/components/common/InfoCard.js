import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class InfoCard extends Component {
    
    iconClass = this.props.iconClass + " text-white";
    
    render() {
        return (
            <div className="card">
                <div className="card-heading p-4">
                    <div className="mini-stat-icon float-right">
                        <i className={this.iconClass} />
                    </div>
                    <div>
                        <h5 className="font-16">{this.props.cardTitle}</h5>
                    </div>
                    <h3 className="mt-4">{this.props.cardBody}</h3>
                    {/* <div className="progress mt-4" style={{ height: 4 }}>
                        <div className="progress-bar bg-primary" role="progressbar" style={{ width: '75%' }} aria-valuenow={75} aria-valuemin={0} aria-valuemax={100} />
                    </div>
                    <p className="text-muted mt-2 mb-0">Previous period<span className="float-right">75%</span></p> */}
                </div>
            </div>
        )
    }
}

InfoCard.propTypes = {
    cardTitle: PropTypes.string.isRequired,
    iconClass: PropTypes.string.isRequired,
    cardBody: PropTypes.node.isRequired
}