import React from "react";
import { Box, Typography, IconButton, Badge } from "@material-ui/core";
import { NotificationsNoneOutlined as NotificationIcon } from "@material-ui/icons";

export default (props) => (
  <Box mb={3} display="flex" justifyContent="space-between" alignItems="center">
    <Typography color="textPrimary" variant="h5">
      {props.title}
    </Typography>
    <Box display="flex" alignItems="center">
      <Box mr={1.5}>
        <Typography variant="body2">Depender Sethi</Typography>
      </Box>
      <IconButton size="small">
        <Badge color="secondary" variant="dot">
          <NotificationIcon />
        </Badge>
      </IconButton>
    </Box>
  </Box>
);
