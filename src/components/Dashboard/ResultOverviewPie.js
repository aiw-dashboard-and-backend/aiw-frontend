import React from "react";
import { Pie } from "react-chartjs-2";

export default (props) => (
  <Pie
    options={{
      legend: {
        display: false,
      },
    }}
    data={{
      datasets: [
        {
          data: [1, 2, 3],
          backgroundColor: ["#50B5FF", "#5A65DC", "#74E3CE"],
          borderWidth: 5,
          barThickness: 2,
        },
      ],
      labels: ["Active", "Completed", "Ended"],
    }}
  />
);
