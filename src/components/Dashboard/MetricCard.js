import React from "react";
import { Box, Typography, makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  wrapper: {
    transition: ".3s",
    cursor: "pointer",
    "&:hover": {
      boxShadow: "0px 2px 10px rgba(0, 0, 0, 0.1)",
    },
  },
});

export default (props) => {
  const classes = useStyles();

  return (
    <Box
      className={classes.wrapper}
      borderRadius="5px"
      bgcolor="white"
      textAlign="center"
      px={2}
      py={3.5}
    >
      <Box mb={1}>
        <Typography color="textSecondary" variant="h4">
          {props.title}
        </Typography>
      </Box>
      <Typography color="textPrimary" variant="body2">
        {props.subtitle}
      </Typography>
    </Box>
  );
};
