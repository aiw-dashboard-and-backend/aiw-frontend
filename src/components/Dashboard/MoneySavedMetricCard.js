import React from "react";
import { Box, Typography, makeStyles } from "@material-ui/core";
import GaugeChart from "react-gauge-chart";

const useStyles = makeStyles({
  wrapper: {
    transition: ".3s",
    cursor: "pointer",
    "&:hover": {
      boxShadow: "0px 2px 10px rgba(0, 0, 0, 0.1)",
    },
  },
});

export default (props) => {
  const classes = useStyles();

  return (
    <Box
      className={classes.wrapper}
      borderRadius="5px"
      display="flex"
      flexDirection="column"
      textAlign="center"
      bgcolor="white"
      px={2}
      boxSizing="border-box"
      height="100%"
    >
      <GaugeChart
        id="gauge-chart3"
        hideText
        style={{ position: "relative", top: "10px" }}
        marginInPercent={0.105}
        nrOfLevels={5}
        colors={["#FF5F6D", "#74E3CE"]}
        arcWidth={0.3}
        textColor="#363636"
        cornerRadius={2}
        percent={0.85}
      />

      <Typography color="textPrimary" variant="body2">
        Money Saved $5,120
      </Typography>
    </Box>
  );
};
