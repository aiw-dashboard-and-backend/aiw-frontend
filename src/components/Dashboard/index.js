import React from "react";
import { Box, Grid, Tooltip, Typography } from "@material-ui/core";
import { Info as InfoIcon } from "@material-ui/icons";
import MetricCard from "./MetricCard";
import BotAnalysisChart from "./BotAnalysisChart";
import BotAnalysisFilled from "./BotAnalysisFilled";
import TaskDistributionDoughnut from "./TaskDistributionDoughnut";
import PageHeader from "../common/PageHeader";
import ResultOverviewPie from "./ResultOverviewPie";
import ChartLegends from "../common/ChartLegends";
import MoneySavedMetricCard from "./MoneySavedMetricCard";

export default (props) => (
  <>
    <PageHeader title="Dashboard" />
    <Grid container spacing={4}>
      <Grid item xs={3}>
        <MetricCard title="$11,320" subtitle="Money saved" />
      </Grid>
      <Grid item xs={3}>
        <MetricCard title="171hrs" subtitle="Time saved" />
      </Grid>
      <Grid item xs={3}>
        <MetricCard title="42" subtitle="FTE Benefits" />
      </Grid>
      <Grid item xs={3}>
        <MoneySavedMetricCard />
      </Grid>
    </Grid>
    <Box mt={2}>
      <Grid container spacing={2} justify="space-between">
        <Grid item xs={8}>
          <Box px={2} pt={3} pb={2} bgcolor="white">
            <Box
              borderRadius="5px"
              mb={3}
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography color="textPrimary" variant="body2">
                Individual Bot Performance
              </Typography>
              <Tooltip
                placement="left"
                title="Information about bot performance"
              >
                <InfoIcon style={{ color: "#414863" }} />
              </Tooltip>
            </Box>
            <BotAnalysisChart />
          </Box>
        </Grid>
        <Grid item xs={4}>
          <Box
            height="100%"
            boxSizing="border-box"
            px={2}
            pt={3}
            pb={2}
            bgcolor="white"
          >
            <Box
              borderRadius="5px"
              mb={3}
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography color="textPrimary" variant="body2">
                Task Distribution
              </Typography>
              <Tooltip placement="left" title="Task Distribution">
                <InfoIcon style={{ color: "#414863" }} />
              </Tooltip>
            </Box>
            <TaskDistributionDoughnut />
            <Box mt={4}>
              <ChartLegends />
            </Box>
          </Box>
        </Grid>
        <Grid item xs={8}>
          <Box px={2} pt={3} pb={2} bgcolor="white">
            <Box
              borderRadius="5px"
              mb={3}
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography color="textPrimary" variant="body2">
                Bots Overview
              </Typography>
              <Tooltip
                placement="left"
                title="Information about bot performance"
              >
                <InfoIcon style={{ color: "#414863" }} />
              </Tooltip>
            </Box>
            <BotAnalysisFilled />
          </Box>
        </Grid>
        <Grid item xs={4}>
          <Box
            height="100%"
            boxSizing="border-box"
            px={2}
            pt={3}
            pb={2}
            bgcolor="white"
          >
            <Box
              borderRadius="5px"
              mb={3}
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography color="textPrimary" variant="body2">
                Result Overview
              </Typography>
              <Tooltip placement="left" title="Result Overview">
                <InfoIcon style={{ color: "#414863" }} />
              </Tooltip>
            </Box>
            <ResultOverviewPie />
            <Box mt={4}>
              <ChartLegends />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  </>
);
