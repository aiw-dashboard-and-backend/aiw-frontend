import React from "react";
import { Bar } from "react-chartjs-2";

export default (props) => {
  return (
    <Bar
      options={{
        legend: {
          display: false,
        },

        scales: {
          xAxes: [
            {
              gridLines: {
                display: false,
              },
            },
          ],
        },
      }}
      data={{
        labels: [
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10,
          11,
          12,
          13,
          14,
          15,
          16,
          17,
          18,
          19,
          20,
        ],
        datasets: [
          {
            data: [
              20,
              10,
              25,
              30,
              40,
              50,
              20,
              35,
              55,
              0,
              35,
              40,
              50,
              30,
              30,
              20,
              30,
              20,
              35,
              40,
              50,
            ],

            backgroundColor: "#02D5FD",
            label: "cool",
            barThickness: 10,
            categoryPercentage: 0.5,
            barPercentage: 1,
          },
          {
            data: [
              25,
              40,
              35,
              40,
              50,
              30,
              30,
              20,
              30,
              15,
              10,
              10,
              25,
              30,
              40,
              50,
              20,
              35,
              30,
              55,
            ],
            backgroundColor: "#3744D7",
            label: "dope",
            barThickness: 10,
            categoryPercentage: 0.5,
            barPercentage: 1,
          },
        ],
      }}
    />
  );
};
