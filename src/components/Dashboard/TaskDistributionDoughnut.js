import React from "react";
import { Box, Typography } from "@material-ui/core";
import { Doughnut } from "react-chartjs-2";

export default (props) => (
  <>
    <Doughnut
      options={{
        cutoutPercentage: 70,
        legend: {
          display: false,
        },
      }}
      data={{
        datasets: [
          {
            data: [1, 2, 3],
            backgroundColor: ["#5C87DC", "#74E3CE", "#F9DB6D"],
            borderWidth: 5,
            barThickness: 2,
          },
        ],
        labels: ["Active", "Completed", "Ended"],
      }}
    />
  </>
);
