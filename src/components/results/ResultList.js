import React, { Component } from 'react'
import { connect } from 'react-redux'

import { fetchDashboardResults, decryptResultsWebSocket } from '../../redux/actions/initActions'

import ResultItem from './ResultItem'

class ResultList extends Component {

    render() {

        if(this.props.encrypted.length !== 0) {
            const enc = this.props.encrypted[0].encrypted
            this.props.decryptResultsWebSocket(enc)
        }
        
        const resultItem = this.props.results.map(result => (
            <ResultItem 
                key = {result.identifier}
                identifierName='Process ID'
                identifier={result.identifier}
                runBadge={result.runBadge}
                runText={result.runText}
            />
        ));

        return (
            <div className="card m-b-30" id = "resultList" >
                <div className="card-body" >
                    <h4 className="mt-0 header-title mb-4">Result overview</h4>
                    <div className="friends-suggestions">
                        {resultItem}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    results: state.init.dashboardResults,
    auth: state.auth.Auth,
    encrypted: state.init.dashboardResultsEncrypted
})

export default connect(mapStateToProps, { fetchDashboardResults, decryptResultsWebSocket })(ResultList)