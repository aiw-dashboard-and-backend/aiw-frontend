import React, { Component } from 'react'

export default class ResultItem extends Component {

    render() {
        return (
            <a href="index.html" className="friends-suggestions-list">
                <div className="border-bottom position-relative">
                    <div className="desc">
                        <h5 className="font-14 mb-1 pt-2">{this.props.identifierName}: <span style = {{fontSize: '12px', fontWeight: 'lighter'}} >{this.props.identifier}</span></h5>
                        <p className="text-muted"><span className={this.props.runBadge}>{this.props.runText}</span></p>
                    </div>
                </div>
            </a>
        )
    }
}