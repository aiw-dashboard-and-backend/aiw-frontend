import React from "react";
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Paper,
  IconButton,
  makeStyles,
} from "@material-ui/core";
import {
  PlayArrow as PlayIcon,
  Stop as StopIcon,
  Videocam as VideoIcon,
} from "@material-ui/icons";
import clsx from "clsx";

const useStyles = makeStyles({
  badge: {
    padding: "5px",
    width: "70px",
    fontSize: "13.5px",
    borderRadius: "25px",
    color: "#363636",
    backgroundColor: "#eee",
    textAlign: "center",
    fontWeight: "bold",
    textTransform: "capitalize",

    "&-Running": {
      backgroundColor: "#41CCB2",
      color: "#fff",
      //   backgroundColor: "rgba(214, 229, 92, .4)",
      //   color: "#bfd322",
    },

    "&-Stopped": {
      backgroundColor: "#FF3131",
      color: "#fff",
      //   backgroundColor: "rgba(255, 175, 17, .4)",
      //   color: "#ffaf11",
    },
    // "&-high": {
    //   backgroundColor: "rgba(216, 67, 48,.1)",
    //   color: "#d84330",
    // },
  },
});

export default (props) => {
  const classes = useStyles();
  return (
    <TableContainer elevation={0} component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Unit ID</TableCell>
            <TableCell>Machine</TableCell>
            <TableCell>State</TableCell>
            <TableCell>Tasks completed</TableCell>
            <TableCell>Runtime</TableCell>
            <TableCell align="center">Actions</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.processes.map((process) => (
            <TableRow key={process.unitId}>
              <TableCell>{process.unitId}</TableCell>
              <TableCell>{process.machine}</TableCell>
              <TableCell>
                <Typography
                  className={clsx(
                    classes.badge,
                    `${classes.badge}-${process.state}`
                  )}
                >
                  {process.state}
                </Typography>
              </TableCell>
              <TableCell>{process.tasksCompleted}</TableCell>
              <TableCell>{process.runtime}</TableCell>
              <TableCell>
                <Box justifyContent="center" display="flex" alignItems="center">
                  <IconButton size="small">
                    <PlayIcon style={{ fontSize: "32px", color: "#363636" }} />
                  </IconButton>
                  <IconButton size="small" style={{ margin: "0 15px" }}>
                    <StopIcon
                      style={{
                        fontSize: "32px",
                        color: "#363636",
                      }}
                    />
                  </IconButton>
                  <IconButton size="small">
                    <VideoIcon style={{ fontSize: "32px", color: "#363636" }} />
                  </IconButton>
                </Box>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
