import React, { useState } from "react";
import { Box, Button, FilledInput } from "@material-ui/core";
import PageHeader from "../common/PageHeader";

import MenuList from "../common/MenuList";
import ProcessesTable from "./ProcessesTable";
import { Search as SearchIcon } from "@material-ui/icons";

const processes = [
  {
    unitId: "#SO-00003",
    machine: "SRV & TPA Bot 1",
    state: "Running",
    tasksCompleted: "40",
    runtime: "2H 23M 4S",
  },
  {
    unitId: "#SO-00005",
    machine: "Gasper Antunes",
    state: "Stopped",
    tasksCompleted: "52",
    runtime: "1H 3M 4S",
  },
];

export default (props) => {
  const [jobPriorAnchor, setJobPriorAnchor] = useState(null);
  const [jobTypeAnchor, setJobTypeAnchor] = useState(null);
  const openMenu = (e, menu) => {
    console.log(menu);
    switch (menu) {
      case "jobPriority":
        return setJobPriorAnchor(e.currentTarget);
      case "jobType":
        return setJobTypeAnchor(e.currentTarget);
      default:
        return;
    }
  };

  return (
    <>
      <PageHeader title="Processes" />
      <Box display="flex">
        <FilledInput
          endAdornment={<SearchIcon />}
          disableUnderline
          placeholder="Search"
        />
        <Box ml={2}>
          <Button color="secondary" variant="contained">
            Add
          </Button>
        </Box>
        <Box ml="auto">
          <MenuList
            anchor={jobPriorAnchor}
            openAnchor={(e) => openMenu(e, "jobPriority")}
            closeAnchor={() => setJobPriorAnchor(null)}
            options={["All", "Important", "Urgent"]}
            label="Job Priority"
          />
          <MenuList
            anchor={jobTypeAnchor}
            openAnchor={(e) => openMenu(e, "jobType")}
            closeAnchor={() => setJobTypeAnchor(null)}
            options={["All", "Amazing"]}
            label="Job Type"
          />
        </Box>
      </Box>
      <Box mt={3}>
        <ProcessesTable processes={processes} />
      </Box>
    </>
  );
};
