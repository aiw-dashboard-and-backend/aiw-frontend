import React, { Component } from 'react'
import { connect } from 'react-redux'

import SweetAlert from 'react-bootstrap-sweetalert'

import { loginUser, hideAlert } from '../../redux/actions/authActions'
export class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            sweetAlert: {
                show: false,
                title: '',
                text: ''
            },
            loginData: {
                username: '',
                password: ''
            }
        }
    }

    hideAlert = () => {
        this.props.hideAlert()
        this.setState({
            loginData: {
                username: '',
                password: ''
            }
        });
    }

    changeHandler = e => {
        this.setState({
            loginData: {
                ...this.state.loginData,
                [e.target.name]: e.target.value
            }
        })
    }

    onSubmit = e => {
        e.preventDefault()
        this.props.loginUser(this.state.loginData)
    }

    render() {

        const { username, password } = this.state.loginData
        return (
            <React.Fragment>
                <div>
                    <div className="accountbg" />
                    <div className="wrapper-page">
                        <div className="card card-pages shadow-none">
                            <div className="card-body" style={{ padding: 40 }} >
                                <div className="text-center m-t-0 m-b-15">
                                    <span className="logo-light" style={{ color: 'white' }} >
                                        <i className="mdi mdi-camera-control" /> Artificially Intelligent Workforce
                                    </span>
                                </div>
                                <h5 className="font-18 text-center">Sign in to continue to FernTech AIW.</h5>
                                <form className="form-horizontal m-t-30" id="loginForm" onSubmit={this.onSubmit} >
                                    <div className="form-group">
                                        <div className="col-12">
                                            <label>Username</label>
                                            <input className="form-control" type="text" required name="username" placeholder="Username" value={username} onChange={this.changeHandler} />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-12">
                                            <label>Password</label>
                                            <input className="form-control" type="password" required name="password" placeholder="Password" value={password} onChange={this.changeHandler} />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="col-12">
                                            <div className="checkbox checkbox-primary">
                                                <div className="custom-control custom-checkbox">
                                                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                                    <label className="custom-control-label" htmlFor="customCheck1"> Remember me</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group text-center m-t-20">
                                        <div className="col-12">
                                            <button className="btn btn-primary btn-block btn-lg waves-effect waves-light" type="submit">Log In</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <SweetAlert
                    type={this.props.sweetAlert.type}
                    title={this.props.sweetAlert.title}
                    onConfirm={this.hideAlert}
                    show={this.props.sweetAlert.show}
                >
                    {this.props.sweetAlert.text}
                </SweetAlert>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => ({
    auth: state.auth.Auth,
    sweetAlert: state.auth.sweetAlert
})

export default connect(mapStateToProps, {loginUser, hideAlert})(Login)
