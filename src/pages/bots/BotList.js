import React, { Component } from 'react'

import { connect } from 'react-redux'

import { 
    fetchDashboardCardsAndCharts,
    connectWebSocket,
    decryptDashboardInfo,
    disconnectWebSocket
} from '../../redux/actions/initActions'

import { connectActionSocket, disconnectActionSocket } from '../../redux/actions/botActions'

import Header from '../../components/base/Header'
import Footer from '../../components/base/Footer'

import PageTitle from '../../components/common/PageTitle'
import InfoCard from '../../components/common/InfoCard'
import BotTable from '../../components/bots/BotTable'

class BotList extends Component {

    crumbs = [
        {
            id: 0,
            name: 'AIW',
            path: '/'
        },
        {
            id: 1,
            name: 'Dashboard',
            path: '/'
        },
        {
            id: 2,
            name: 'Bots',
            path: '/bots'
        }
    ]

    componentDidMount = () => {
        this.props.fetchDashboardCardsAndCharts('bankAsia');
        this.props.connectWebSocket('sarvodaya');
        this.props.connectActionSocket('sarvodaya')
    }

    componentWillUnmount = () => {
        this.props.disconnectWebSocket('sarvodaya')
        this.props.disconnectActionSocket('sarvodaya')
    }

    render() {

        if(this.props.encrypted.length !== 0) {
            const enc = this.props.encrypted[0].encrypted
            this.props.decryptDashboardInfo(enc)
        }

        return (
            <React.Fragment>
                <Header />
                <div className="wrapper">
                    <div className="container-fluid">
                        <PageTitle title = "Bots" crumbs = {this.crumbs} ></PageTitle>
                        {/* Info cards start */}
                        <div className="row">
                            <div className="col-sm-12 col-xl-6">
                                <InfoCard 
                                    cardTitle="Average Runtime"
                                    iconClass="mdi mdi-camera-timer bg-warning"
                                    cardBody={this.props.cards.runtime}
                                />
                            </div>
                            <div className="col-sm-12 col-xl-6">
                                <InfoCard 
                                    cardTitle="Bots Running"
                                    iconClass="fas fa-robot bg-danger"
                                    cardBody={this.props.cards.botsRunning}                                
                                />
                            </div>
                        </div>
                        {/* Info cards end */}

                        {/* Provision new bots start */}
                        {/* <div className="row justify-content-end">
                            <div className = "col-md-3" >
                                <button type="button" className="btn btn-lg btn-primary waves-effect waves-light" style={{ width: '100%' }}>Provision new bot <i className="fas fa-plus-square" /> </button>
                            </div>
                        </div> */}
                        <br></br>

                        {/* Provision new bots end */}

                        {/* Bot list start */}
                        <div className="row">
                            <div className="col-12">
                                <BotTable></BotTable>
                            </div> {/* end col */}
                        </div> {/* end row */}
                        {/* Bot list end */}
                    </div>
                    {/* end container-fluid */}
                </div>
                <Footer />
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    cards: state.init.dashboardCardInfo,
    encrypted: state.init.dashboardInfoEncrypted
})

export default connect(
                mapStateToProps, 
                {
                    fetchDashboardCardsAndCharts,
                    connectWebSocket,
                    decryptDashboardInfo,
                    disconnectWebSocket,
                    connectActionSocket,
                    disconnectActionSocket
                }
                )(BotList);