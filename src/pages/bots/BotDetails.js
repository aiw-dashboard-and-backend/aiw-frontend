import React, { Component } from "react";

import { connect } from "react-redux";
import // fetchDashboardAreaChart
"../../redux/actions/initActions";

import Header from "../../components/base/Header";
import Footer from "../../components/base/Footer";
import PageTitle from "../../components/common/PageTitle";
import ActivityList from "../../components/activity/ActivityList";
// import AreaChart from "../../components/charts/PerformanceChart";
import BotDetailsCard from "../../components/bots/BotDetailsCard";
import BotActionCard from "../../components/bots/BotActionCard";
import Logtable from "../../components/bots/LogTable";

class BotDetails extends Component {
  crumbs = [
    {
      id: 0,
      name: "AIW",
      path: "/",
    },
    {
      id: 1,
      name: "Dashboard",
      path: "/",
    },
    {
      id: 2,
      name: "Bots",
      path: "/bots",
    },
    {
      id: 3,
      name: "Bot Details",
      path: "/bots/:id",
    },
  ];

  componentDidMount = () => {
    // this.props.fetchDashboardAreaChart('sarvodaya');
  };

  render() {
    return (
      <React.Fragment>
        <Header />
        <div className="wrapper">
          <div className="container-fluid">
            {/* Page-Title */}
            <PageTitle title="Bot Details" crumbs={this.crumbs} />
            {/* Info cards start */}
            <div className="row row-eq-height">
              <div className="col-xl-6">
                <BotDetailsCard></BotDetailsCard>
              </div>
              <div className="col-xl-6">
                <BotActionCard></BotActionCard>
              </div>
            </div>
            {/* Info cards end */}
            {/* Detailed insight begins */}
            {/* Control panel and recent activity begin */}
            <div className="row">
              <div className="col-xl-12">
                <ActivityList></ActivityList>
              </div>
            </div>
            {/* Control panel and recent activity ends */}
            {/* Performance graph */}
            <div className="row">
              <div className="col-xl-12">
                {/* <AreaChart chartData={this.props.areaChart}></AreaChart> */}
              </div>
            </div>
            {/* Performance graph ends */}
            {/* Bot list start */}
            <div className="row">
              <div className="col-12">
                <Logtable></Logtable>
              </div>{" "}
              {/* end col */}
            </div>{" "}
            {/* end row */}
            {/* Bot list end */}
            {/* Detailed insight ends */}
          </div>
          {/* end container-fluid */}
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    areaChart: state.init.areaChart,
  };
};

export default connect(mapStateToProps, {
  // fetchDashboardAreaChart
})(BotDetails);
