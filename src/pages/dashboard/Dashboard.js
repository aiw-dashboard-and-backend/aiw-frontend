import React, { Component } from "react";

import { connect } from "react-redux";
import {
  fetchDashboardCardsAndCharts,
  fetchDashboardActivity,
  fetchDashboardResults,
  connectWebSocket,
  connectLogsWebSocket,
  decryptDashboardInfo,
  disconnectLogsWebSocket,
  disconnectWebSocket,
} from "../../redux/actions/initActions";

// User imports
import Header from "../../components/base/Header";
import Footer from "../../components/base/Footer";
import PageTitle from "../../components/common/PageTitle";
import InfoCard from "../../components/common/InfoCard";
import AreaChart from "../../components/charts/PerformanceChart";
import DonutChart from "../../components/charts/DonutChart";
import ResultList from "../../components/results/ResultList";
import ActivityList from "../../components/activity/ActivityList";

class Dashboard extends Component {
  crumbs = [
    {
      id: 0,
      name: "AIW",
      path: "/",
    },
    {
      id: 1,
      name: "Dashboard",
      path: "/",
    },
  ];

  componentDidMount = () => {
    this.props.connectWebSocket("sarvodaya");
    this.props.connectLogsWebSocket("sarvodaya");
    this.props.fetchDashboardCardsAndCharts("bankAsia");
    this.props.fetchDashboardActivity("bankAsia");
    this.props.fetchDashboardResults("bankAsia");
  };

  componentWillUnmount = () => {
    this.props.disconnectLogsWebSocket("sarvodaya");
    this.props.disconnectWebSocket("sarvodaya");
  };

  render() {
    if (this.props.encrypted.length !== 0) {
      const enc = this.props.encrypted[0].encrypted;
      this.props.decryptDashboardInfo(enc);
    }

    return (
      <React.Fragment>
        <Header></Header>
        <div className="wrapper">
          <div className="container-fluid">
            <PageTitle title={"Dashboard"} crumbs={this.crumbs}></PageTitle>
            <div className="row">
              <div className="col-sm-6 col-xl-3">
                <InfoCard
                  cardTitle="Tasks Processed"
                  iconClass="mdi mdi-factory bg-primary"
                  cardBody={this.props.cards.tasksProcessed}
                />
              </div>
              <div className="col-sm-6 col-xl-3">
                <InfoCard
                  cardTitle="Success Rate"
                  iconClass="mdi mdi-briefcase-check bg-success"
                  cardBody={this.props.cards.successRate}
                />
              </div>
              <div className="col-sm-6 col-xl-3">
                <InfoCard
                  cardTitle="Average Runtime"
                  iconClass="mdi mdi-camera-timer bg-warning"
                  cardBody={this.props.cards.runtime}
                />
              </div>
              <div className="col-sm-6 col-xl-3">
                <InfoCard
                  cardTitle="Bots Running"
                  iconClass="fas fa-robot bg-danger"
                  cardBody={this.props.cards.botsRunning}
                />
              </div>
              {/* <div className="col-sm-6 col-xl-6">
                                <InfoCard 
                                    cardTitle="FTE Benefits: Money saved"
                                    iconClass="fas fa-dollar-sign bg-info"
                                    cardBody="USD 20,500"
                                />
                            </div>
                            <div className="col-sm-6 col-xl-6">
                                <InfoCard 
                                    cardTitle="FTE Benefits: Time saved"
                                    iconClass="fas fa-clock bg-dark"
                                    cardBody="20 Hours"
                                />
                            </div> */}
            </div>
            <div className="row">
              <div className="col-xl-8">
                <AreaChart
                  chartData={this.props.areaChart.data}
                  botNames={this.props.areaChart.labels}
                  lineColors={this.props.areaChart.lineColors}
                ></AreaChart>
              </div>
              <div className="col-xl-4">
                <DonutChart pieData={this.props.pieData}></DonutChart>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-4">
                <ResultList></ResultList>
              </div>
              <div className="col-xl-8">
                <ActivityList></ActivityList>
              </div>
            </div>
          </div>
        </div>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  cards: state.init.dashboardCardInfo,
  areaChart: state.init.areaChart,
  pieData: state.init.pieData,
  auth: state.auth.Auth,
  dashboardActivity: state.init.dashboardActivity,
  encrypted: state.init.dashboardInfoEncrypted,
});

export default connect(mapStateToProps, {
  fetchDashboardCardsAndCharts,
  fetchDashboardActivity,
  fetchDashboardResults,
  connectWebSocket,
  connectLogsWebSocket,
  decryptDashboardInfo,
  disconnectLogsWebSocket,
  disconnectWebSocket,
})(Dashboard);
