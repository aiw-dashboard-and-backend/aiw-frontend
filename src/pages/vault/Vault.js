import React, { useState } from "react";
// User imports
import Header from "../../components/base/Header";
import Footer from "../../components/base/Footer";
import { connect } from "react-redux";
import AddKeyCard from "./AddKeyCard";
import KeyCard from "./KeyCard";
import NewKeyModal from "./NewKeyModal";

const Vault = () => {
  const [keys, setKeys] = useState([]);

  const handleCreateKey = ({ name, value }) =>
    setKeys((k) => [...k, { id: Math.random().toString(), name, value }]);

  const handleDeleteKey = (id) =>
    window.confirm("You sure bruh") &&
    setKeys((keys) => keys.filter((k) => k.id !== id));

  return (
    <>
      <Header></Header>
      <div className="wrapper">
        <NewKeyModal createKey={handleCreateKey} />
        <div className="container-fluid mt-3">
          <div className="d-flex align-items-center">
            <img className="mr-3" src="/assets/images/vault.png" alt="vault" />
            <div className="d-flex flex-column mr-5">
              <h5>Vault</h5>
              <p>Secure and easy way to access keys</p>
            </div>
            <button
              data-toggle="modal"
              data-target="#exampleModalCenter"
              className="btn btn-green"
            >
              Create key
            </button>
          </div>
        </div>
        <div className="container-fluid mt-2">
          <div className="row row-eq-height">
            {keys.map((k) => (
              <KeyCard
                key={k.id}
                name={k.name}
                value={k.value}
                deleteKey={() => handleDeleteKey(k.id)}
              />
            ))}
            <AddKeyCard />
          </div>
        </div>
      </div>
      <Footer></Footer>
    </>
  );
};

const mapState = ({ reports }) => ({ reports });

export default connect(mapState)(Vault);
