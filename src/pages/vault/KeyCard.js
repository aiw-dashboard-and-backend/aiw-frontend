import React from "react";

export default ({ name, value, deleteKey }) => (
  <div className="col-sm-3 key">
    <div className="key-content">
      <div className="d-flex flex-column key-details">
        <div className="key-name mb-1">{name}</div>
        <div className="key-value">
          <i className="fas fa-key" /> {value}
        </div>
      </div>
      <i onClick={deleteKey} className="fas fa-trash delete-icon"></i>
    </div>
  </div>
);
