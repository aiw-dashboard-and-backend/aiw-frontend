import React from "react";

export default ({ name }) => (
  <div
    className="col-sm-3 add-key"
    data-toggle="modal"
    data-target="#exampleModalCenter"
  >
    <div className="key-content">
      <i className="far fa-plus-square mr-2"></i> Add Key
    </div>
  </div>
);
