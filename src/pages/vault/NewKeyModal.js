import React, { useState } from "react";
import { useRef } from "react";

const initDetails = {
  name: "",
  value: "",
};

export default ({ createKey, deleteKey }) => {
  const [details, setDetails] = useState(initDetails);
  const btnRef = useRef();

  const handleChange = (e) => {
    e.persist();
    setDetails((old) => ({ ...old, [e.target.name]: e.target.value }));
  };

  const handleSubmit = (e) => {
    createKey(details);
    setDetails(initDetails);
    btnRef.current.click();
  };

  return (
    <div
      className="modal fade"
      id="exampleModalCenter"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalCenterTitle"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLongTitle">
              Create key pass
            </h5>

            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              ref={btnRef}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <input
              type="text"
              name="name"
              autoComplete="off"
              value={details.name}
              className="form-control mb-3"
              placeholder="Key name"
              onChange={handleChange}
            />
            <input
              type="text"
              name="value"
              autoComplete="off"
              value={details.value}
              className="form-control"
              placeholder="Key value"
              onChange={handleChange}
            />
          </div>
          <div className="modal-footer d-flex">
            <button
              type="button"
              className="btn btn-green flex-grow"
              onClick={handleSubmit}
            >
              Add Key
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
