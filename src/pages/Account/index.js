import React, { Component } from "react";

import Header from "../../components/base/Header";
import Footer from "../../components/base/Footer";
import PageTitle from "../../components/common/PageTitle";
import UserPNG from "../../components/assets/images/users/user-4.jpg";

class User extends Component {
  crumbs = [
    {
      id: 0,
      name: "AIW",
      path: "/",
    },
    // {
    //   id: 1,
    //   name: "Dashboard",
    //   path: "/",
    // },
    {
      id: 2,
      name: "Account",
      path: "/account",
    },
  ];

  componentDidMount = () => {
    // this.props.fetchDashboardAreaChart('sarvodaya');
  };

  render() {
    return (
      <>
        <Header />
        <div className="wrapper">
          <div className="container-fluid">
            {/* Page-Title */}
            <PageTitle title="Your account" crumbs={this.crumbs} />

            <div className="row row-eq-height mb-5 align-items-center">
              <div className="col-xs-2 mr-3">
                <img src={UserPNG} alt="user" className="ml-3 rounded-circle" />
              </div>
              <div className="col-xs-2">
                <button className="btn btn-success">Change</button>
              </div>
            </div>

            <h6>Invite Members</h6>
            <div className="row row-eq-height mb-4">
              <div className="col-lg-3 col-sm-4">
                <input
                  placeholder="Email"
                  type="text"
                  className="form-control"
                />
              </div>
              <div className="col-lg-3 col-sm-4">
                <select
                  className="form-control"
                  name="role"
                  placeholder="Role"
                  id=""
                >
                  <option value="role1">Role 1</option>
                  <option value="role2">Role 2</option>
                  <option value="role3">Role 3</option>
                </select>
              </div>
            </div>

            <h6>Change Password</h6>
            <div className="row row-eq-height mb-3">
              <div className="col-lg-3 col-sm-4">
                <input
                  placeholder="Password"
                  type="text"
                  className="form-control"
                />
              </div>
              <div className="col-lg-3 col-sm-4">
                <input
                  placeholder="Re-enter Password"
                  type="text"
                  className="form-control"
                />
              </div>
            </div>
            <button className="btn btn-primary mb-4">Change Password</button>
          </div>
        </div>

        <Footer />
      </>
    );
  }
}

export default User;
