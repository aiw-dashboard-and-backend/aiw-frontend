import React from "react";
import Datatable from "react-bs-datatable";

const DataTable = (props) => {
  return (
    <Datatable
      tableHeaders={props.headers}
      tableBody={props.body}
      // tableClass="table table-bordered dt-responsive nowrap"
      classes={{
        paginationOptsForm: { display: "none" },
      }}
      rowsPerPage={10}
      keyName="key"
    />
  );
};

export default DataTable;
