import React from "react";
import CsvDownload from "react-json-to-csv";
import PdfReport from "./PdfReport";
import { PDFDownloadLink } from "@react-pdf/renderer";
import DataTable from "./DataTable";

export default ({ fetched, data, headers }) =>
  fetched ? (
    data.length ? (
      <>
        <div className="row mb-3">
          <div className="col">
            <CsvDownload
              data={data}
              filename="report.csv"
              className="mr-2 btn btn-success"
            >
              Export to CSV
            </CsvDownload>
            <PDFDownloadLink
              document={<PdfReport headers={headers} body={data} />}
              fileName="report.pdf"
            >
              <button className="btn btn-danger">Export to PDF</button>
            </PDFDownloadLink>
          </div>
        </div>
        <DataTable body={data} headers={headers} />
      </>
    ) : (
      "No data available"
    )
  ) : (
    "Loading..."
  );
