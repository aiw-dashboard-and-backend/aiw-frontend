import React, { useState, useRef } from "react";
import {
  Box,
  Select,
  MenuItem,
  withStyles,
  Chip,
  Popper,
  Paper,
  makeStyles,
  ClickAwayListener,
  Button,
} from "@material-ui/core";
import { Edit as EditIcon } from "@material-ui/icons";

import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import "./search.css";
import { DateRangePicker } from "react-dates";

const useStyles = makeStyles({
  popper: {
    position: "relative",
    top: "10px",
    width: "220px",
    paddingTop: "8px",
  },
  verifiedOpts: {
    "&>*": {
      border: "1px solid #000",
      textAlign: "center",
      color: "#000",
      padding: "5px 12.5px",
      marginBottom: "8px",
      fontSize: "13.5px",
      borderRadius: "20px",
      cursor: "pointer",
      trasition: "all .3s",
    },
    "&>*:hover": {
      backgroundColor: "#000",
      color: "#fff",
    },
  },
  verifiedOptSelected: {
    backgroundColor: "#000",
    color: "#fff",
  },
  btn: {
    textTransform: "capitalize",
    fontWeight: 600,
    borderRadius: "2px",
    backgroundColor: "#00dbd3",
    padding: "6px 28px",
  },
});

const SelectList = withStyles({
  root: {
    color: "#fff",
    backgroundColor: "#222437",
  },
  filled: {
    padding: "17px 12px 10px",
  },
  icon: {
    color: "#fff",
  },
})(Select);

const verifiedOpts = ["Successful", "Unsuccessful", "Both"];

export default (props) => {
  const classes = useStyles();
  const [form, setForm] = useState({
    startDate: null,
    endDate: null,
    focusedInput: null,
    verified: "Both",
  });

  const [verifiedPopper, setVerifiedPopper] = useState(false);

  const verifiedEl = useRef(null);

  return (
    <Box mb={4} display="flex" justifyContent="space-between">
      <Box display="flex" alignItems="center">
        <DateRangePicker
          startDate={form.startDate}
          startDateId="your_unique_start_date_id"
          endDate={form.endDate}
          endDateId="your_unique_end_date_id"
          onDatesChange={({ startDate, endDate }) =>
            setForm((o) => ({ ...o, startDate, endDate }))
          }
          focusedInput={form.focusedInput}
          onFocusChange={(focusedInput) =>
            setForm((o) => ({ ...o, focusedInput }))
          }
        />
        <Box ml={3.5}>
          <SelectList disableUnderline variant="filled" defaultValue="branch1">
            <MenuItem disabled>Branch</MenuItem>
            <MenuItem value="branch1">Branch 1</MenuItem>
            <MenuItem value="branch2">Branch 2</MenuItem>
            <MenuItem value="branch3">Branch 3</MenuItem>
          </SelectList>
        </Box>
        <Box
          ref={verifiedEl}
          display="flex"
          alignItems="center"
          ml={3.5}
          fontSize="16"
          color="#fff"
        >
          Verified:
          <Box ml={2}>
            <Chip
              label={form.verified}
              onClick={() => setVerifiedPopper(true)}
              deleteIcon={<EditIcon />}
              onDelete={() => {}}
            />
          </Box>
        </Box>
        <Popper
          open={verifiedPopper}
          anchorEl={verifiedEl.current}
          placement="bottom"
          transition
        >
          <ClickAwayListener onClickAway={() => setVerifiedPopper(false)}>
            <Paper className={classes.popper}>
              <Box p={1} className={classes.verifiedOpts}>
                {verifiedOpts.map((opt) => (
                  <Box
                    key={opt}
                    onClick={() => setForm((o) => ({ ...o, verified: opt }))}
                    className={
                      form.verified === opt ? classes.verifiedOptSelected : ""
                    }
                  >
                    {opt}
                  </Box>
                ))}
              </Box>
            </Paper>
          </ClickAwayListener>
        </Popper>
      </Box>
      <Box>
        <Button className={classes.btn} variant="contained" disableElevation>
          Search
        </Button>
      </Box>
    </Box>
  );
};
