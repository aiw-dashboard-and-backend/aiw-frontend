import React, { Component } from "react";

// User imports
import Header from "../../components/base/Header";
import Footer from "../../components/base/Footer";
import PageTitle from "../../components/common/PageTitle";
import { connect } from "react-redux";

// import Sheet from "../../components/sheet/Sheet";
import { fetchIntimationReport } from "../../redux/actions/reportsActions";
import ReportGeneration from "./ReportGeneration";
import SearchBar from "./SearchBar/SearchBar";

class Intimation extends Component {
  crumbs = [
    {
      id: 0,
      name: "AIW",
      path: "/",
    },
    {
      id: 1,
      name: "Dashboard",
      path: "/",
    },
    {
      id: 2,
      name: "Bill Intimation",
      path: "/reports/intimation",
    },
  ];

  componentDidMount() {
    !this.props.reports.intimationFetched && this.props.fetchIntimationReport();
  }

  render() {
    const { intimation, intimationFetched } = this.props.reports;
    const headers = intimation.length
      ? Object.keys(intimation[0]).map((h) => ({
          title: h.split("_").join(" "),
          prop: h,
          filterable: true,
          sortable: true,
        }))
      : [];
    return (
      <React.Fragment>
        <Header></Header>
        <div className="wrapper">
          <div className="container-fluid">
            <PageTitle
              title={"Bill Intimation"}
              crumbs={this.crumbs}
            ></PageTitle>
            <SearchBar />
            <ReportGeneration
              fetched={intimationFetched}
              data={intimation}
              headers={headers}
            />
          </div>
        </div>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

const mapState = ({ reports }) => ({ reports });

export default connect(mapState, { fetchIntimationReport })(Intimation);
