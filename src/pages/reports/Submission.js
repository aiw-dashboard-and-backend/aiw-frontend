import React, { Component } from "react";

// User imports
import Header from "../../components/base/Header";
import Footer from "../../components/base/Footer";
import PageTitle from "../../components/common/PageTitle";

// import Sheet from '../../components/sheet/Sheet'
import ReportGeneration from "./ReportGeneration";

import { connect } from "react-redux";
import { fetchSubmissionReport } from "../../redux/actions/reportsActions";

class Submission extends Component {
  crumbs = [
    {
      id: 0,
      name: "AIW",
      path: "/",
    },
    {
      id: 1,
      name: "Dashboard",
      path: "/",
    },
    {
      id: 2,
      name: "Bill Submission",
      path: "/reports/submission",
    },
  ];

  componentDidMount() {
    !this.props.reports.submissionFetched && this.props.fetchSubmissionReport();
  }

  render() {
    const { submission, submissionFetched } = this.props.reports;
    const headers = submission.length
      ? Object.keys(submission[0]).map((h) => ({
          title: h.split("_").join(" "),
          prop: h,
          filterable: true,
          sortable: true,
        }))
      : [];
    return (
      <React.Fragment>
        <Header></Header>
        <div className="wrapper">
          <div className="container-fluid">
            <PageTitle title="Bill Submission" crumbs={this.crumbs}></PageTitle>
            <ReportGeneration
              fetched={submissionFetched}
              data={submission}
              headers={headers}
            />
          </div>
        </div>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

const mapState = ({ reports }) => ({ reports });
export default connect(mapState, { fetchSubmissionReport })(Submission);
