import React from "react";
import { Document, StyleSheet, Image, View, Page } from "@react-pdf/renderer";
import {
  Table,
  TableCell,
  TableHeader,
  TableBody,
  DataTableCell,
} from "@david.kucsai/react-pdf-table";

const styles = StyleSheet.create({
  body: {
    paddingTop: 25,
    paddingHorizontal: 25,
    paddingBottom: 69,
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
  },
  tc: {
    padding: "5px",
    fontSize: 10,
  },
  image: {
    // width: "138.33px",
    height: "35px",
    position: "absolute",
    bottom: 13,
    left: 15,
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  logo2: {
    position: "relative",
    right: -15,
  },
});

export default (props) => (
  <Document>
    <Page style={styles.body}>
      <Table data={props.body}>
        <TableHeader>
          {props.headers.map((h) => (
            <TableCell style={styles.tc} key={h.title}>
              {h.title}
            </TableCell>
          ))}
        </TableHeader>
        <TableBody>
          {Object.keys(props.body[0]).map((c) => (
            <DataTableCell key={c} style={styles.tc} getContent={(r) => r[c]} />
          ))}
        </TableBody>
      </Table>
      <View fixed style={styles.image}>
        <Image src="/assets/images/pdf-footer-aiw.png" />
        <Image
          style={styles.logo2}
          src="/assets/images/pdf-footer-ferntech.png"
        />
      </View>
    </Page>
  </Document>
);
