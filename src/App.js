import React, { useEffect } from "react";
// import "./app.css";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { Provider } from "react-redux";
import { Box, ThemeProvider } from "@material-ui/core";
import store from "./redux/store";
import theme from "./theme/theme";

import Startup from "./components/startup/Startup";
import SideNav from "./components/SideNav";

// New pages
import Dashboard from "./components/Dashboard";
import Processes from "./components/Processes";
import Queues from "./components/Queues";
import Triggers from "./components/Triggers";

// Old pages
// import Dashboard from "./pages/dashboard/Dashboard";
// import BotList from "./pages/bots/BotList";
// import Submission from "./pages/reports/Submission";
// import Intimation from "./pages/reports/Intimation";
// import BotDetails from "./pages/bots/BotDetails";
// import Account from "./pages/Account";
// import Vault from "./pages/vault/Vault";

function App() {
  useEffect(() => {
    document.body.style = `background-color: ${theme.palette.background.default}`;
  }, []);

  return (
    <Provider store={store}>
      <Startup>
        <ThemeProvider theme={theme}>
          <Router>
            <SideNav />
            <Box ml={22} p={5} pt={2} pr={2}>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/processes" component={Processes} />
              <Route exact path="/queues" component={Queues} />
              <Route exact path="/triggers" component={Triggers} />
              {/* <Route exact path="/bots/:botId" component={BotDetails} />
            <Route exact path="/reports/intimation" component={Intimation} />
            <Route exact path="/reports/submission" component={Submission} />
            <Route exact path="/bots" component={BotList} />
            <Route exact path="/account" component={Account} />
            <Route exact path="/vault" component={Vault} /> */}
            </Box>
          </Router>
        </ThemeProvider>
      </Startup>
    </Provider>
  );
}

export default App;
