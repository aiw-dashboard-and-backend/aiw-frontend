import forge from 'node-forge'
import { Base64 } from 'js-base64'

import encryptionCert from '../../certificates/sarvodaya_client_public.pem'
import decryptionCert from '../../certificates/sarvodaya_client_private.pem'

export const encryptData = data => {
    return fetch(encryptionCert)
        .then(r => r.text())
        .then(cert => {
            cert = Base64.decode(cert)
            let senderKey = forge.pki.publicKeyFromPem(cert)
            let sessionKey = forge.random.getBytesSync(16).toString('binary')

            let encSessionKey = senderKey.encrypt(sessionKey, "RSA-OAEP", {
                md: forge.md.sha256.create(),
                mgf1: forge.mgf.mgf1.create()
            })

            let cipherAES = forge.cipher.createCipher('AES-GCM', sessionKey)
            let iv = forge.random.getBytesSync(16)
            cipherAES.start({
                iv: iv,
            })
            cipherAES.update(forge.util.createBuffer(JSON.stringify(data)))
            let pass = cipherAES.finish()

            if (pass) {
                let encrypted = {
                    "enc_session_key": (Base64.btoa(encSessionKey)),
                    "nonce": (Base64.btoa(iv)),
                    "tag": (Base64.btoa(cipherAES.mode.tag.data)),
                    "ciphertext": (Base64.btoa(cipherAES.output.data))
                }
                return JSON.stringify(encrypted)
            }

            return false
        })
}

export const decryptData = data => {
    return fetch(decryptionCert)
        .then(r => r.text())
        .then(cert => {
            cert = Base64.decode(cert)
            let privateKey = forge.pki.privateKeyFromPem(cert)

            let encryptedData = JSON.parse(data)

            const encSessionKey = Base64.atob(forge.util.encodeUtf8(encryptedData.enc_session_key))
            const nonce = Base64.atob(forge.util.encodeUtf8(encryptedData.nonce))
            const tag = Base64.atob(forge.util.encodeUtf8(encryptedData.tag))
            const ciphertext = Base64.atob(forge.util.encodeUtf8(encryptedData.ciphertext))

            let sessionKey = privateKey.decrypt(encSessionKey, "RSA-OAEP", {
                md: forge.md.sha256.create(),
                mgf1: forge.mgf.mgf1.create()
            })

            let cipherAES = forge.cipher.createDecipher('AES-GCM', sessionKey)
            cipherAES.start({
                iv: nonce,
                tag: tag
            })
            cipherAES.update(forge.util.createBuffer(ciphertext))
            let pass = cipherAES.finish()

            if (pass) {
                const decrypted = cipherAES.output.data
                return JSON.parse(decrypted)
            }

            return false
        })
}