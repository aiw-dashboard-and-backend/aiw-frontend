import {
    FETCH_BOT_LIST,
    ACTIONS_WEBSOCKET_BROKEN,
    ACTIONS_WEBSOCKET_CLOSED,
    ACTIONS_WEBSOCKET_CONNECT,
    ACTIONS_WEBSOCKET_DISCONNECT,
    ACTIONS_WEBSOCKET_MESSAGE,
    ACTIONS_WEBSOCKET_OPEN,
    ACTIONS_WEBSOCKET_SEND,
} from '../actions/types'

const initialState = {
    botList: [],
}

export default function (state = initialState, action) {
    switch (action.type) {

        case FETCH_BOT_LIST:
            return {
                ...state,
                botList: action.payload
            }
        case ACTIONS_WEBSOCKET_CONNECT:
            return {
                ...state,
            };
        case ACTIONS_WEBSOCKET_OPEN:
            return {
                ...state,
            };
        case ACTIONS_WEBSOCKET_BROKEN:
        case ACTIONS_WEBSOCKET_CLOSED:
            return {
                ...state,
            };

        case ACTIONS_WEBSOCKET_MESSAGE:
            const res = action.payload.message
            let data = JSON.parse(res)
            data = data.data
            let payload = []
            data.forEach(data => {

                let runtimeArray, hours, minutes, seconds
                if (data.totalRuntime !== 'None') {
                    runtimeArray = data.totalRuntime.split(":");
                    hours = runtimeArray[0];
                    minutes = runtimeArray[1];
                    seconds = runtimeArray[2];
                } else {
                    hours = 0
                    minutes = 0
                    seconds = 0
                }
                let runBadge, runText;
                if (data.status === 1) {
                    runText = 'Running';
                    runBadge = 'badge badge-success'
                }
                else {
                    runText = 'Stopped';
                    runBadge = 'badge badge-danger'
                }

                let result = {
                    'unitID': data.unitID,
                    'botName': data.botName,
                    'runBadge': runBadge,
                    'runText': runText,
                    'tasksCompleted': data.tasksCompleted,
                    'totalRuntime': hours + "H " + minutes + "M " + Math.floor(seconds) + "S"
                }
                payload.push(result)
            })
            return {
                ...state,
                botList: payload
            };

        case ACTIONS_WEBSOCKET_DISCONNECT:
            return {
                ...state,
            }
        case ACTIONS_WEBSOCKET_SEND:
            return {
                ...state,
            }
        
        default:
            return state;

    }
}