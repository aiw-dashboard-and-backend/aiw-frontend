import { TEST } from '../actions/types'

const initialState = {
    items: [],
    item: {}
}

export default function(state = initialState, action) {
    switch(action.type) {
        case TEST:
            // console.log(action.payload);            
            return {
                ...state,
                items: action.payload
            }
        default: 
            return state;
    }
}