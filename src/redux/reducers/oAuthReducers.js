import {
  FETCH_OAUTH_TOKEN,
  LOGIN_USER,
  HIDE_ALERT,
  LOGOUT_USER,
} from "../actions/types";

const initialState = {
  Auth: {},
  sweetAlert: {
    show: false,
    title: "",
    text: "",
  },
  role: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_OAUTH_TOKEN:
      return {
        ...state,
        Auth: action.payload,
      };
    case LOGIN_USER:
      return {
        ...state,
        Auth: action.payload.auth,
        sweetAlert: action.payload.sweetAlert,
        role: action.payload.role,
      };
    case LOGOUT_USER:
      return {
        ...state,
        Auth: action.payload.auth,
        sweetAlert: action.payload.sweetAlert,
        role: action.payload.role,
      };
    case HIDE_ALERT:
      return {
        ...state,
        sweetAlert: action.payload,
      };
    default:
      return state;
  }
}
