import {
  FETCH_INTIMATION_REPORT,
  FETCH_SUBMISSION_REPORT,
} from "../actions/types";

const initialState = {
  intimation: [],
  submission: [],
  intimationFetched: false,
  submissionFetched: false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_INTIMATION_REPORT:
      return {
        ...state,
        intimation: action.payload,
        intimationFetched: true,
      };
    case FETCH_SUBMISSION_REPORT:
      return {
        ...state,
        submission: action.payload,
        submissionFetched: true,
      };
    default:
      return state;
  }
}
