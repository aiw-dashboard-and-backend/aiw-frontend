import { combineReducers } from "redux";
import initReducers from "./initReducers";
import oAuthReducers from "./oAuthReducers";
import botReducer from "./botReducer";
import reportsReducer from "./reportsReducer";

export default combineReducers({
  init: initReducers,
  auth: oAuthReducers,
  bots: botReducer,
  reports: reportsReducer,
});
