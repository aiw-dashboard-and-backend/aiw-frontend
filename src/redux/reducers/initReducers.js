import {
    FETCH_DASHBOARD_CARDS,
    FETCH_DASHBOARD_AREACHART,
    FETCH_DASHBOARD_DONUTCHART,
    FETCH_DASHBOARD_RESULTS,
    FETCH_DASHBOARD_ACTIVITY,
    RESULTS_WEBSOCKET_BROKEN,
    RESULTS_WEBSOCKET_CLOSED,
    RESULTS_WEBSOCKET_CONNECT,
    RESULTS_WEBSOCKET_MESSAGE,
    RESULTS_WEBSOCKET_OPEN,
    LOGS_WEBSOCKET_BROKEN,
    LOGS_WEBSOCKET_CLOSED,
    LOGS_WEBSOCKET_CONNECT,
    LOGS_WEBSOCKET_MESSAGE,
    LOGS_WEBSOCKET_OPEN,
    LOGS_WEBSOCKET_MESSAGE_DECRYPTED,
    RESULTS_WEBSOCKET_MESSAGE_DECRYPTED,
    RESULTS_WEBSOCKET_MESSAGE_DASHBOARD_DECRYPTED
} from '../actions/types'

const initialState = {
    dashboardCardInfo: {
        tasksProcessed: '0',
        successRate: '0%',
        runtime: '0M 00S',
        botsRunning: '0'
    },
    areaChart: {
        element: 'morris-area',
        pointSize: 0,
        lineWidth: 0,
        data: [],
        xkey: '',
        ykeys: [],
        labels: [],
        lineColors: []
    },
    pieData: [
        { botName: 'Group A', value: 400 },
        { botName: 'Group B', value: 300 },
    ],
    dashboardResults: [{
        identifierName: 'Process ID',
        identifier: '',
        runText: '',
        runBadge: ''
    }],
    dashboardActivity: [{
        botName: '',
        timestamp: '',
        message: ''
    }],
    dashboardActivityEncrypted: [],
    dashboardResultsEncrypted: [],
    dashboardInfoEncrypted: [],
    dashboardSocketURL: '',
    dashboardSocketStatus: false,
    dashboardSocketMessages: [],
    logSocketURL: '',
    logSocketStatus: false,
    logSocketMessages: []    
}

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_DASHBOARD_CARDS:

            return {
                ...state,
                dashboardCardInfo: {
                    tasksProcessed: action.payload.tasksProcessed,
                    successRate: action.payload.successRate,
                    runtime: action.payload.runtime,
                    botsRunning: action.payload.botsRunning
                },
                areaChart: action.payload.chartData,
                pieData: action.payload.pieData
            }
        case FETCH_DASHBOARD_AREACHART:
            return {
                ...state,
                areaChart: action.payload
            }
        case FETCH_DASHBOARD_DONUTCHART:
            return {
                ...state,
                donutChart: action.payload
            }
        case FETCH_DASHBOARD_RESULTS:
            return {
                ...state,
                dashboardResults: action.payload
            }
        case FETCH_DASHBOARD_ACTIVITY:
            return {
                ...state,
                dashboardActivity: action.payload
            }            
        case RESULTS_WEBSOCKET_CONNECT:
            return {
                ...state,
                dashboardSocketURL: action.payload.url,
            };
        case RESULTS_WEBSOCKET_OPEN:
            return {
                ...state,
                dashboardSocketStatus: true,
            };
        case RESULTS_WEBSOCKET_BROKEN:
        case RESULTS_WEBSOCKET_CLOSED:
            return {
                ...state,
                dashboardSocketStatus: false,
            };

        case RESULTS_WEBSOCKET_MESSAGE:
            
            return {
                ...state,
                dashboardSocketMessages: [
                    ...state.dashboardSocketMessages,
                    {
                        data: JSON.parse(action.payload.message),
                        origin: action.payload.origin,
                        timestamp: action.meta.timestamp,
                        type: 'INCOMING',
                    },
                ],
                dashboardResultsEncrypted: [
                    {
                        encrypted: action.payload.message,
                    },
                    ...state.dashboardResults
                ],
                dashboardInfoEncrypted: [
                    {
                        encrypted: action.payload.message,
                    }
                ]
            };

        case RESULTS_WEBSOCKET_MESSAGE_DECRYPTED:
            const dResults = action.payload

            let runBadge, runText;
            if(dResults.success === 2){
                runText = 'Success';
                runBadge = 'badge badge-success'
            }
            else if(dResults.success === 1){
                runText = 'Pending';
                runBadge = 'badge badge-warning'
            }
            else {
                runText = 'Failed';
                runBadge = 'badge badge-danger'
            }

            return {
                ...state,
                dashboardResults: [
                    {
                        'identifierName': 'Process ID',
                        'identifier': dResults.processID,
                        'runBadge': runBadge,
                        'runText': runText
                    },
                    ...state.dashboardResults
                ],
                dashboardResultsEncrypted: []
            };
        
        case RESULTS_WEBSOCKET_MESSAGE_DASHBOARD_DECRYPTED:

            const runtimeArray = action.payload.averageRuntime.split(":");
            // const hours = runtimeArray[0];
            const minutes = runtimeArray[1];
            const seconds = runtimeArray[2];

            const areaData = action.payload.chartData.areaData;

            const chartData = {
                data: areaData,
                xkey: action.payload.chartData.xkey,
                labels: action.payload.chartData.labels,
                lineColors: ['#fcbe2d', '#30419b', '#02c58d']
            }
            
            return {
                ...state,
                areaChart: chartData,
                pieData: action.payload.chartData.taskMap,
                dashboardCardInfo: {
                    tasksProcessed: action.payload.tasksProcessed,
                    successRate: action.payload.successRate + '%',
                    runtime: minutes + 'M ' + Math.floor(seconds) + 'S',
                    botsRunning: action.payload.runningBots + '/' + action.payload.totalLicenses,
                },
                dashboardInfoEncrypted: []
            }

        case LOGS_WEBSOCKET_CONNECT:
            return {
                ...state,
                logSocketURL: action.payload.url,
            };
        case LOGS_WEBSOCKET_OPEN:
            return {
                ...state,
                logSocketStatus: true,
            };
        case LOGS_WEBSOCKET_BROKEN:
        case LOGS_WEBSOCKET_CLOSED:
            return {
                ...state,
                logSocketStatus: false,
            };

        case LOGS_WEBSOCKET_MESSAGE:
            
            return {
                ...state,
                logSocketMessages: [
                    ...state.logSocketMessages,
                    {
                        data: JSON.parse(action.payload.message),
                        origin: action.payload.origin,
                        timestamp: action.meta.timestamp,
                        type: 'INCOMING',
                    },
                ],
                dashboardActivityEncrypted: [
                    {
                        'encrypted': action.payload.message
                    },
                    ...state.dashboardActivity
                ]            
            };
        
        case LOGS_WEBSOCKET_MESSAGE_DECRYPTED:
            return {
                ...state,
                dashboardActivity: [
                    {
                        'botName': action.payload.botName,
                        'timestamp': action.payload.timestamp,
                        'message': action.payload.message
                    },
                    ...state.dashboardActivity
                ],
                dashboardActivityEncrypted: []
            }

        default:
            return state;
    }
}