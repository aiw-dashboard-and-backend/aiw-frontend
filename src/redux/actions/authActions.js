import {
    LOGIN_USER,
    HIDE_ALERT,
    LOGOUT_USER
} from './types'

const axios = require('axios');

export const loginUser = loginData => dispatch => {
    axios.post(process.env.REACT_APP_BACKEND_URL + "api/auth/login/", loginData)
        .then(response => {
            let res = JSON.parse(response.data)

            let payload = {
                auth: {},
                sweetAlert: {}
            }

            if (!res.success) {
                payload.auth = {}
                payload.sweetAlert = {
                    show: true,
                    title: 'Error!',
                    type: 'danger',
                    text: res.message
                }
            }
            else {
                payload.auth = JSON.parse(res.tokenData)
                payload.role = res.role
                payload.sweetAlert = {
                    show: false,
                    title: '',
                    text: ''
                }
            }

            dispatch({
                type: LOGIN_USER,
                payload: payload
            });
        })
        .catch(error => {
            console.error(error)
        })

}

export const hideAlert = () => dispatch => {

    let payload = {
        show: false,
        title: '',
        text: ''
    }

    dispatch({
        type: HIDE_ALERT,
        payload: payload
    })

}

export const logoutUser = () => dispatch => {

    const payload = {
        auth: null,
        sweetAlert: {
            show: false,
            title: '',
            text: ''        
        },
        role: null
    }

    dispatch({
        type: LOGOUT_USER,
        payload: payload
    })
    
}