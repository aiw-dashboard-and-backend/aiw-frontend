import { TEST } from './types'

export const testCheck = () => dispatch => {
    console.log("In actions/testActions/testCheck");
    fetch('https://jsonplaceholder.typicode.com/posts')
        .then(res => res.json())
        .then(data => dispatch({
            type: TEST,
            payload: data
        }))
}