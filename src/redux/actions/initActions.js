import {
    FETCH_DASHBOARD_CARDS,
    FETCH_DASHBOARD_RESULTS,
    FETCH_DASHBOARD_ACTIVITY,
    LOGS_WEBSOCKET_MESSAGE_DECRYPTED,
    RESULTS_WEBSOCKET_MESSAGE_DECRYPTED,
    RESULTS_WEBSOCKET_MESSAGE_DASHBOARD_DECRYPTED
} from './types'

import { connect, disconnect } from '@giantmachines/redux-websocket';

import { decryptData } from '../helpers/cryptoHelper'

const axios = require('axios')
// Called after receiving OAuth token
export const fetchDashboardCardsAndCharts = (group) => (dispatch, getState) => {

    // Get OAuth access token from state
    const { access_token } = getState().auth.Auth;

    var axiosConfig = {
        headers: { 'Authorization': 'Bearer ' + access_token }
    };

    // Performing a GET request
    axios.get(process.env.REACT_APP_BACKEND_URL + 'api/dashboard/', axiosConfig)
        .then(res => {

            let data = JSON.stringify(res.data);
            decryptData(data)
                .then(data => {
                    let runtimeArray, minutes, seconds
                    if(data.averageRuntime !== 'None') {
                        runtimeArray = data.averageRuntime.split(":");
                        // hours = runtimeArray[0];
                        minutes = runtimeArray[1];
                        seconds = runtimeArray[2];
                    } else {
                        // hours = 0
                        minutes = 0
                        seconds = 0
                    }

                    const areaData = data.chartData.areaData;
                    const pieData = data.chartData.taskMap;

                    const chartData = {
                        data: areaData,
                        xkey: data.chartData.xkey,
                        labels: data.chartData.labels,
                        lineColors: ['#fcbe2d', '#30419b', '#02c58d']
                    }

                    let payload = {
                        tasksProcessed: data.tasksProcessed,
                        successRate: data.successRate + '%',
                        runtime: minutes + 'M ' + Math.floor(seconds) + 'S',
                        botsRunning: data.runningBots + '/' + data.totalLicenses,
                        chartData: chartData,
                        pieData: pieData
                    }

                    dispatch({
                        type: FETCH_DASHBOARD_CARDS,
                        payload: payload
                    });
                })
        })
}

export const fetchDashboardResults = group => (dispatch, getState) => {

    // Get OAuth access token from state
    const { access_token } = getState().auth.Auth;

    var axiosConfig = {
        headers: { 'Authorization': 'Bearer ' + access_token }
    };

    axios.get(process.env.REACT_APP_BACKEND_URL + 'api/results/?limit=5', axiosConfig)
        .then(res => {
            // console.log(res.data);

            let data = JSON.stringify(res.data);
            decryptData(data)
                .then(data => {

                    data = data.results

                    let results = []

                    data.map(result => {
                        let runBadge, runText;
                        if (result.success === 2) {
                            runText = 'Success';
                            runBadge = 'badge badge-success'
                        }
                        else {
                            runText = 'Failed';
                            runBadge = 'badge badge-danger'
                        }

                        results = [
                            ...results,
                            {
                                'identifierName': result.identifier,
                                'identifier': result.id,
                                'runBadge': runBadge,
                                'runText': runText
                            }
                        ]

                        return results;
                    });

                    dispatch({
                        type: FETCH_DASHBOARD_RESULTS,
                        payload: results
                    });
                })

        })
}

export const fetchDashboardActivity = group => (dispatch, getState) => {

    // Get OAuth access token from state
    const { access_token } = getState().auth.Auth;

    var axiosConfig = {
        headers: { 'Authorization': 'Bearer ' + access_token }
    };

    axios.get(process.env.REACT_APP_BACKEND_URL + 'api/logs/?limit=50', axiosConfig)
        .then(res => {
            let data = JSON.stringify(res.data)

            decryptData(data)
                .then(data => {

                    data = data.results

                    let results = []

                    data.map(result => {

                        let timestamp = new Date(result.timestamp);

                        results = [
                            ...results,
                            {
                                'botName': result.botName,
                                'timestamp': timestamp.toLocaleDateString() + ' ' + timestamp.toLocaleTimeString(),
                                'message': result.message
                            }
                        ];
                        return results;

                    });

                    dispatch({
                        type: FETCH_DASHBOARD_ACTIVITY,
                        payload: results
                    });

                })
        })
}

export const connectWebSocket = group => dispatch => {
    dispatch(connect('ws://' + process.env.REACT_APP_SOCKET_DOMAIN + '/ws/result/sarvodaya/all', 'RESULTS_WEBSOCKET'));
}

export const connectLogsWebSocket = group => dispatch => {
    dispatch(connect('ws://' + process.env.REACT_APP_SOCKET_DOMAIN + '/ws/log/sarvodaya/all', 'LOGS_WEBSOCKET'));
}

export const disconnectWebSocket = group => dispatch => {
    dispatch(disconnect('RESULTS_WEBSOCKET'))
}

export const disconnectLogsWebSocket = group => dispatch => {
    dispatch(disconnect('LOGS_WEBSOCKET'))
}

export const decryptLogsWebSocket = encrypted => dispatch => {
    decryptData(encrypted)
        .then(decrypted => {
            dispatch({
                type: LOGS_WEBSOCKET_MESSAGE_DECRYPTED,
                payload: decrypted
            })
        })
}

export const decryptResultsWebSocket = encrypted => dispatch => {
    decryptData(encrypted)
        .then(decrypted => {
            dispatch({
                type: RESULTS_WEBSOCKET_MESSAGE_DECRYPTED,
                payload: decrypted
            })
        })
}

export const decryptDashboardInfo = encrypted => dispatch => {
    decryptData(encrypted)
        .then(decrypted => {
            dispatch({
                type: RESULTS_WEBSOCKET_MESSAGE_DASHBOARD_DECRYPTED,
                payload: decrypted
            })
        })    
}