import { FETCH_INTIMATION_REPORT, FETCH_SUBMISSION_REPORT } from "./types";
import axios from "axios";
import { json2 } from "../../pages/reports/data";

export const fetchIntimationReport = () => (dispatch, getState) => {
  // Get OAuth access token from state
  const { access_token } = getState().auth.Auth;

  var axiosConfig = {
    headers: { Authorization: "Bearer " + access_token },
  };

  // replace json with data
  axios
    .get(process.env.REACT_APP_BACKEND_URL + "api/nidreport/", axiosConfig)
    .then((res) =>
      dispatch({
        type: FETCH_INTIMATION_REPORT,
        payload: res.data,
      })
    );
};

export const fetchSubmissionReport = () => async (dispatch) => {
  // replace json with data
  // const data = await axios.get(process.env.REACT_APP_SUBMISSION_REPORT_API);
  dispatch({
    type: FETCH_SUBMISSION_REPORT,
    payload: json2,
  });
};
