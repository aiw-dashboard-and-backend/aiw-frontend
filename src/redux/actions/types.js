import {
  WEBSOCKET_BROKEN,
  WEBSOCKET_CLOSED,
  WEBSOCKET_CONNECT,
  WEBSOCKET_DISCONNECT,
  WEBSOCKET_MESSAGE,
  WEBSOCKET_OPEN,
  WEBSOCKET_SEND,
} from "@giantmachines/redux-websocket";

import {
  RESULTS_WEBSOCKET_PREFIX,
  LOGS_WEBSOCKET_PREFIX,
  ACTIONS_WEBSOCKET_PREFIX,
} from "../constants";

export const FETCH_INTIMATION_REPORT = "FETCH_INTIMATION_REPORT";
export const FETCH_SUBMISSION_REPORT = "FETCH_SUBMISSION_REPORT";

export const TEST = "TEST";

export const FETCH_OAUTH_TOKEN = "FETCH_OAUTH_TOKEN";
export const LOGIN_USER = "LOGIN_USER";
export const LOGOUT_USER = "LOGOUT_USER";
export const HIDE_ALERT = "HIDE_ALERT";
export const FETCH_CERTIFICATES = "FETCH_CERTIFICATES";

export const FETCH_DASHBOARD_CARDS = "FETCH_DASHBOARD_CARDS";
export const FETCH_DASHBOARD_AREACHART = "FETCH_DASHBOARD_AREACHART";
export const FETCH_DASHBOARD_DONUTCHART = "FETCH_DASHBOARD_DONUTCHART";
export const FETCH_DASHBOARD_RESULTS = "FETCH_DASHBOARD_RESULTS";
export const FETCH_DASHBOARD_ACTIVITY = "FETCH_DASHBOARD_ACTIVITY";

export const RESULTS_WEBSOCKET_BROKEN = `${RESULTS_WEBSOCKET_PREFIX}::${WEBSOCKET_BROKEN}`;
export const RESULTS_WEBSOCKET_OPEN = `${RESULTS_WEBSOCKET_PREFIX}::${WEBSOCKET_OPEN}`;
export const RESULTS_WEBSOCKET_CLOSED = `${RESULTS_WEBSOCKET_PREFIX}::${WEBSOCKET_CLOSED}`;
export const RESULTS_WEBSOCKET_MESSAGE = `${RESULTS_WEBSOCKET_PREFIX}::${WEBSOCKET_MESSAGE}`;
export const RESULTS_WEBSOCKET_CONNECT = `${RESULTS_WEBSOCKET_PREFIX}::${WEBSOCKET_CONNECT}`;
export const RESULTS_WEBSOCKET_DISCONNECT = `${RESULTS_WEBSOCKET_PREFIX}::${WEBSOCKET_DISCONNECT}`;
export const RESULTS_WEBSOCKET_SEND = `${RESULTS_WEBSOCKET_PREFIX}::${WEBSOCKET_SEND}`;

export const LOGS_WEBSOCKET_BROKEN = `${LOGS_WEBSOCKET_PREFIX}::${WEBSOCKET_BROKEN}`;
export const LOGS_WEBSOCKET_OPEN = `${LOGS_WEBSOCKET_PREFIX}::${WEBSOCKET_OPEN}`;
export const LOGS_WEBSOCKET_CLOSED = `${LOGS_WEBSOCKET_PREFIX}::${WEBSOCKET_CLOSED}`;
export const LOGS_WEBSOCKET_MESSAGE = `${LOGS_WEBSOCKET_PREFIX}::${WEBSOCKET_MESSAGE}`;
export const LOGS_WEBSOCKET_CONNECT = `${LOGS_WEBSOCKET_PREFIX}::${WEBSOCKET_CONNECT}`;
export const LOGS_WEBSOCKET_DISCONNECT = `${LOGS_WEBSOCKET_PREFIX}::${WEBSOCKET_DISCONNECT}`;
export const LOGS_WEBSOCKET_SEND = `${LOGS_WEBSOCKET_PREFIX}::${WEBSOCKET_SEND}`;

export const ACTIONS_WEBSOCKET_BROKEN = `${ACTIONS_WEBSOCKET_PREFIX}::${WEBSOCKET_BROKEN}`;
export const ACTIONS_WEBSOCKET_OPEN = `${ACTIONS_WEBSOCKET_PREFIX}::${WEBSOCKET_OPEN}`;
export const ACTIONS_WEBSOCKET_CLOSED = `${ACTIONS_WEBSOCKET_PREFIX}::${WEBSOCKET_CLOSED}`;
export const ACTIONS_WEBSOCKET_MESSAGE = `${ACTIONS_WEBSOCKET_PREFIX}::${WEBSOCKET_MESSAGE}`;
export const ACTIONS_WEBSOCKET_CONNECT = `${ACTIONS_WEBSOCKET_PREFIX}::${WEBSOCKET_CONNECT}`;
export const ACTIONS_WEBSOCKET_DISCONNECT = `${ACTIONS_WEBSOCKET_PREFIX}::${WEBSOCKET_DISCONNECT}`;
export const ACTIONS_WEBSOCKET_SEND = `${ACTIONS_WEBSOCKET_PREFIX}::${WEBSOCKET_SEND}`;

export const LOGS_WEBSOCKET_MESSAGE_DECRYPTED =
  "LOGS_WEBSOCKET_MESSAGE_DECRYPTED";
export const RESULTS_WEBSOCKET_MESSAGE_DECRYPTED =
  "RESULTS_WEBSOCKET_MESSAGE_DECRYPTED";
export const RESULTS_WEBSOCKET_MESSAGE_DASHBOARD_DECRYPTED =
  "RESULTS_WEBSOCKET_MESSAGE_DASHBOARD_DECRYPTED";

export const FETCH_BOT_LIST = "FETCH_BOT_LIST";
