import {
    FETCH_BOT_LIST
} from './types'

import { connect, disconnect, send } from '@giantmachines/redux-websocket';

import { decryptData } from '../helpers/cryptoHelper'

const axios = require('axios')

export const fetchBotList = () => (dispatch, getState) => {

    const { access_token } = getState().auth.Auth;

    var axiosConfig = {
        headers: { 'Authorization': 'Bearer ' + access_token }
    };

    axios.get(process.env.REACT_APP_BACKEND_URL + 'api/botlist/', axiosConfig)
        .then(res => {

            let data = JSON.stringify(res.data);
            decryptData(data)
                .then(data => {
                    let payload = []
                    data.forEach(data => {

                        let runtimeArray, hours, minutes, seconds
                        if (data.totalRuntime !== 'None') {
                            runtimeArray = data.totalRuntime.split(":");
                            hours = runtimeArray[0];
                            minutes = runtimeArray[1];
                            seconds = runtimeArray[2];
                        } else {
                            hours = 0
                            minutes = 0
                            seconds = 0
                        }
                        let runBadge, runText;
                        if (data.status === 1) {
                            runText = 'Running';
                            runBadge = 'badge badge-success'
                        }
                        else {
                            runText = 'Stopped';
                            runBadge = 'badge badge-danger'
                        }

                        let result = {
                            'unitID': data.unitID,
                            'botName': data.botName,
                            'runBadge': runBadge,
                            'runText': runText,
                            'tasksCompleted': data.tasksCompleted,
                            'totalRuntime': hours + "H " + minutes + "M " + Math.floor(seconds) + "S"
                        }
                        payload.push(result)
                    })
                    dispatch({
                        type: FETCH_BOT_LIST,
                        payload: payload
                    });
                })
        })
}

export const connectActionSocket = group => dispatch => {
    dispatch(connect('ws://' + process.env.REACT_APP_SOCKET_DOMAIN + '/ws/action/sarvodaya/action', 'ACTIONS_WEBSOCKET'));
}

export const disconnectActionSocket = group => dispatch => {
    dispatch(disconnect('ACTIONS_WEBSOCKET'))
}

export const sendAction = data => dispatch => {
    dispatch(send(data, 'ACTIONS_WEBSOCKET'))
}