import { createStore, applyMiddleware } from "redux";

import thunk from "redux-thunk";
import reduxWebsocket from "@giantmachines/redux-websocket";
import reduxCookiesMiddleware, {
  getStateFromCookies,
} from "redux-cookies-middleware";

import Cookies from "js-cookie";

import rootReducer from "./reducers";

let initialState = {
  auth: {
    Auth: {},
    sweetAlert: {
      show: false,
      title: "",
      text: "",
    },
    role: "",
  },
};
// Create the middleware instance.
const resultsWebsocketMiddleware = reduxWebsocket({
  prefix: "RESULTS_WEBSOCKET",
  reconnectOnClose: true,
});
const logsWebsocketMiddleware = reduxWebsocket({
  prefix: "LOGS_WEBSOCKET",
  reconnectOnClose: true,
});

const actionsWebsocketMiddleware = reduxWebsocket({
  prefix: "ACTIONS_WEBSOCKET",
  reconnectOnClose: true,
});

// Cookie middleware

const setCookie = (name, value) => {
  let expiry = new Date(new Date().getTime() + 600 * 60 * 1000);
  Cookies.set(name, value, { expires: expiry });
};

const getCookie = (name) => {
  return Cookies.get(name);
};

// state to persist in cookies
const paths = {
  "auth.Auth": { name: "aiw_token" },
  "auth.role": { name: "aiw_role" },
};

// read stored data in cookies and merge it with the initial state
initialState = getStateFromCookies(initialState, paths, (name) =>
  getCookie(name)
);

const middleware = [
  thunk,
  resultsWebsocketMiddleware,
  logsWebsocketMiddleware,
  actionsWebsocketMiddleware,
  reduxCookiesMiddleware(paths, {
    setCookie: (name, value) => setCookie(name, value),
  }),
];

const store = createStore(
  rootReducer,
  initialState,
  applyMiddleware(...middleware)
);

export default store;
